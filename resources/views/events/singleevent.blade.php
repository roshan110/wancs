@extends('layouts.app')
@section('title') {{ $event->title }}
@endsection

@section('content')
    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $event->title }}</h2>
                        <ul class="page-title-link">
                            <li><a href="{{ route('_root_') }}">Home</a></li>
                            <li><a href="#">{{ $event->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="about" class="section wb">
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="message-box text-justify">
                                    <div class="row">
                                            @if($event->image != null)
                                                <div class="col-md-12">
                                                    <div class="service-widget">
                                                        <div class="post-media wow fadeIn">
                                                            <a href="{{ asset('uploads/events/'.$event->image) }}" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                                            <img src="{{ asset('uploads/events/'.$event->image) }}" alt="" class="img-responsive img-rounded">
                                                        </div>
                                                        <div class="service-dit">
                                                            <h3><a href="">{{ $event->title }}</a></h3>
                                                            <h6 class="">{{ $event->created_at->format('M d, Y') }}</h6>
                                                            {!! $event->details !!}
                                                        </div>
                                                    </div>
                                                </div>

                                            @else
                                                <div class="col-md-4">
                                                    <div class="service-widget">
                                                        <div class="service-dit">
                                                            <h3><a href="#">{{ $event->title }}</a></h3>
                                                            <h6 class="">{{ $event->created_at->format('M d, Y') }}</h6>
                                                            {!! $event->details !!}
                                                        </div>
                                                    </div>
                                                </div>

                                            @endif

                                    </div>

                                </div><!-- end messagebox -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end container -->
                </div>
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>




@endsection