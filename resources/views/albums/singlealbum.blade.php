@extends('layouts.app')
@section('title') {{ $album->title }}
@endsection

@section('content')
    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $album->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div><br>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                @if($album->photos()->count())
                    <div class="row" id="lightgallery">
                        @foreach($album->photos as $photo)
                        <a class="col-md-3" style="margin-bottom: 20px;" href="{{ asset('uploads/photo/' . $photo->image) }}" title="{{ $photo->caption }}">
                            <img src="{{ $photo->image('400x300') }}" width="200" height="180" alt="{{ $photo->caption }}" class="img-fluid">
                        </a>
                        @endforeach
                    </div>
                @else
                    <p>No photos available in this album.</p>
                @endif
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
@endsection

@section('script')
<link rel="stylesheet" href="{{ asset('vendors/lightgallery/lightgallery.min.css') }}">
<script src="{{ asset('vendors/lightgallery/lightgallery.min.js') }}"></script>
<script src="{{ asset('vendors/lightgallery/lg-zoom.js') }}"></script>
<script>
    $(document).ready(function() {
        lightGallery(document.getElementById('lightgallery'),{
            download:true,
            // zoom: true,
        });
    });
</script>
@endsection