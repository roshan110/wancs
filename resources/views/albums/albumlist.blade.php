@extends('layouts.app')
@section('title') {{ $page->title }}
@endsection

@section('content')
    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $page->title }}</h2>
                        <ul class="page-title-link">
                            <li><a href="{{ route('_root_') }}">Home</a></li>
                            <li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="portfolio" class="section wb">
                    <div>
                        <div class="section-title text-center">
                            <h3>{{ $page->title }}</h3>
                        </div><!-- end title -->
                    </div><!-- end container -->

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#photo">Photo</a></li>
                                    <li><a href="#video" data-toggle="pill">Video</a></li>
                                </ul>
                            </div>
                        </div>

                        <hr class="invis">

                        <div class="tab-content">
                            <div id="photo" class="tab-pane fade in active">
                                @foreach($albums as $album)
                                    <div class="col-md-4">
                                        <div class="card" style="margin-bottom:30px;">
                                            <div>
                                                <img class="card-img-top" src="{{ $album->thumb }}" alt="{{ $album->title }}" width="250" height="180">
                                            </div><br>
                                            <div class="card-body" style="height:70px">
                                                <h2 style="text-transform:none;"><a href="{{ route('single.gallery',$album->slug) }}">{{ $album->title }}</a></h2>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div id="video" class="tab-pane fade">
                                <div class="row">
                                    @foreach ($videos as $video)
                                        <div class="col-md-4">
                                            <iframe width="250" height="180" src="https://www.youtube.com/embed/{{ $video->video_id }}" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div><!-- end container -->
                </div>
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
@endsection