@extends('layouts.app')

@section('title') {{ $page->title }} @endsection

@section('content')
	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>{{ $page->title }}</h2>
						<ul class="page-title-link">
							<li><a href="{{ route('_root_') }}">Home</a></li>
							<li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="test-box" class="section wb">
                    <div>
                        <div class="section-title text-center">
                            <h3>{{ \App\Label::ofValue('global:testimonial_title') }}</h3>
                            <p class="lead">{{ \App\Label::ofValue('global:testimonial_detail') }}</p>
                        </div><!-- end title -->

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="testi-carousel owl-carousel owl-theme">
                                    @foreach($reviews as $review)
                                        <div class="testimonial clearfix">
                                            <div class="desc text-justify">
                                                <h3><i class="fa fa-quote-left"></i>{{ $review->title }}</h3>
                                                {!! $review->details !!}
                                                <h3>{{ $review->name }} <small>- Manager of Racer</small></h3>
                                            </div>
                                            <!-- end testi-meta -->
                                        </div>
                                        <!-- end testimonial -->
                                    @endforeach
                                </div><!-- end carousel -->
                            </div><!-- end col -->
                        </div><!-- end row -->

                    </div><!-- end container -->
                </div>
            </div>

            <div class="col-md-3">


                    <div id="test-box" class="section wb">
                        <div>
                <div class="section-title text-center">
                    <h3>Write Review</h3>

                </div><!-- end title -->

                <form action="{{ route('review') }}" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                    </div>
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="">Photo (optional)</label>
                        <input type="file" name="image">
                    </div>

                    <div class="form-group">
                        <label for="">Review</label>
                        <textarea name="review" id="review" class="form-control" value="{{ old('review') }}" rows="5"></textarea>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <img src="{{ route('captcha') }}" alt="Captcha">
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control text {{ $errors->has('captcha') ? ' is-invalid' : '' }}" id="captcha" name="captcha" placeholder="Enter Captcha" required="">
                            @if ($errors->has('captcha'))
                                <span class="help-block text-danger"><strong>{{ $errors->first('captcha') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </form>
                        </div>
                        </div>
            </div>
        </div>
    </div>
@endsection