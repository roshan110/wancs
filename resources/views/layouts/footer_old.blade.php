<!-- copyright -->
<div class="copy_right py-4">
    <div class="container">
        <div class="row">
            <div class="col-md-auto mr-md-auto">
                <p class="text-white">© 2018 {{ config('app.name') }}. All rights reserved</p>
            </div>
            <div class="col-sm text-center footer_link">
                @foreach ($footer_menu as $item)
                    <a href="{{ $item->url }}" class="btn btn-link">{{ $item->title }}</a>
                @endforeach
            </div>

            <div class="col-md-auto ml-md-auto">
                <p class="text-white">Design by: <a href="http://w3web.co.uk/" target="_blank">W3 Web Technology</a></p>
            </div>
        </div>
    </div>
</div>

<!-- //copyright -->

<div class="cookie_message" id="cookie_message">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <p>We use cookies to make your experience better. To comply with the new e-Privacy directive, we need to ask
                    for your consent to set the cookies.</p>
            </div>
            <div class="col-sm-3">
                <button id="cookie_yes" class="button button-secondary button-small">Yes I consent </button>
                <a href="#" class="button button-plain button-small">Learn More</a>
            </div>
        </div>
    </div>
</div>