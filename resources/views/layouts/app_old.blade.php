<!DOCTYPE html>
<html>

<head>
    <title> @yield('title') | {{ config('app.name') }}</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('image/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <script>
        addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
    </script>
    <!--// Meta tag Keywords -->

    <!-- Web-Fonts -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:300" rel="stylesheet">


    <!-- Custom-Files -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <!-- Bootstrap-Core-CSS -->

    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/component.css') }}" />

    <link rel="stylesheet" href="{{ asset('stylesheets/app.css') }}?ref={{ str_random() }}" type="text/css" media="all" />
    <style>
        .publication{
            padding: 10px;
            border: 1px #EEE solid;
            box-shadow: 0px 10px 15px -3px #CCC;
            position: relative;
        }
    </style>

</head>

<body>
    @include('layouts.header') 
    @yield('content')
    @include('layouts.footer')


    <!-- Js files -->
    <!-- JavaScript -->
    <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <!-- Default-JavaScript-File -->
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <!-- Necessary-JavaScript-File-For-Bootstrap -->

    <!-- flexSlider (for testimonials) -->
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen" property="" />
    <script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script>
        $(window).load(function () {
			$('.flexslider').flexslider({
				animation: "slide",
				start: function (slider) {
					$('body').removeClass('loading');
				}
			});
		});
    </script>
    <!-- //flexSlider (for testimonials) -->

    <!-- stats -->
    <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/jquery.countup.js') }}"></script>
    <script>
        $('.counter').countUp();
    </script>
    <!-- //stats -->

    <!-- password-script -->
    <script>
        // window.onload = function () {
		// 	document.getElementById("password1").onchange = validatePassword;
		// 	document.getElementById("password2").onchange = validatePassword;
		// }

		// function validatePassword() {
		// 	var pass2 = document.getElementById("password2").value;
		// 	var pass1 = document.getElementById("password1").value;
		// 	if (pass1 != pass2)
		// 		document.getElementById("password2").setCustomValidity("Passwords Don't Match");
		// 	else
		// 		document.getElementById("password2").setCustomValidity('');
		// 	//empty string means no validation error
		// }
    </script>
    <script>
        $(function () {
			cbpBGSlideshow.init();
		});
    </script>
    <!-- background slider -->

    <!-- //password-script -->

    <!-- smooth scrolling -->
    <script src="{{ asset('js/SmoothScroll.min.js') }}"></script>
    <!-- //smooth scrolling -->

    <!-- move-top -->
    <script src="{{ asset('js/move-top.js') }}"></script>
    <!-- easing -->
    <script src="{{ asset('js/easing.js') }}"></script>
    <!--  necessary snippets for few javascript files -->
    <script src="{{ asset('js/district.js') }}"></script>

    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <!-- Necessary-JavaScript-File-For-Bootstrap -->
    <script src="{{ asset('js/jquery.imagesloaded.min.js') }}"></script>
    <script src="{{ asset('js/cbpBGSlideshow.min.js') }}"></script>
    <script src="{{ asset('vendors/owl/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendors/countdown/jquery.countdown.min.js') }}"></script>

    <script>
        $('.homeslide').owlCarousel({
			loop: true,
			margin: 10,
			nav: false,
			items: 1,
            autoplay:true,
            autoplayTimeout:7000,
		})
    </script>

    <script src="{{ asset('vendors/jquery.cookie.js') }}"></script>

    <script>
        $(function () {
			//cookies message
			$('#cookie_yes').on('click', function () {
				$.cookie('moneytree_cookie_message11', 'yes', {
					expiry: 0,
					domain: '',
					path: ''
				});
				$('#cookie_message').slideToggle('hide');
			});
			if (typeof $.cookie('moneytree_cookie_message11') == 'undefined') {
				$('#cookie_message').slideToggle('slow');
			}
		});
    </script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=451729431830506&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    @yield('scripts')

</body>

</html>