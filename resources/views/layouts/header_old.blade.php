<header>
    <div class="header_topw3layouts_bar border-bottom">
        <div class="container">
            <!-- header-top -->
            <div class="row py-1">
                <div class="col-lg-5 header_agileits_left">
                    <ul>
                        <li class="mr-3">
                            <i class="fas fa-phone mr-2"></i>{{ Label::ofValue('global:mobile') }}</li>
                        <li class="mr-3">
                            <i class="fas fa-envelope mr-2"></i>
                            <a href="mailto:{{ Label::ofValue('global:email') }}">{{ Label::ofValue('global:email') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-7 header_right mt-lg-0 mt-2">
                    <div class="row">
                        <!-- social icons -->
                        <div class="col-sm-auto w3social-icons">
                            <ul>
                                @foreach ($socials as $social)
                                    <li class="px-2"><a href="{{ $social->link }}" class="rounded-circle"><i class="fab fa-{{ $social->icon }}"></i></a></li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="col ml-md-auto header-loginw3ls">
                            <a href="#" class="button button-secondary button-small button-raised button-pill"> Log In</a>
                            <a href="#" class="button button-plain button-small button-raised button-pill" data-toggle="modal" data-target="#volunteer">Register</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--// header-top -->
        </div>
    </div>
</header>

<div class="mainmenu">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{ route('_root_') }}"> <img src="{{ asset('images/logo.jpg') }}" alt="{{ config('app.name') }}" height="100"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">

                    @foreach ($main_menu as $link=>$menu)
                        @if (is_array($menu))
                            @if($menu[$link] == "Courses")
                                <li class="nav-item mx-lg-2 my-lg-0 my-3 dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">{{ $menu[$link] }}</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @foreach ($menu['sub'] as $sub_link=>$sub_menu)
                                            <a class="dropdown-item" href="{{ url('courses/'.$sub_link) }}">{{ $sub_menu }}</a>
                                        @endforeach
                                    </div>
                                </li>
                            @else
                                <li class="nav-item mx-lg-2 my-lg-0 my-3 dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">{{ $menu[$link] }}</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @foreach ($menu['sub'] as $sub_link=>$sub_menu)
                                            <a class="dropdown-item" href="{{ url($sub_link) }}">{{ $sub_menu }}</a>
                                        @endforeach
                                    </div>
                                </li>
                            @endif
                        @else
                            <li class="nav-item mx-lg-2 my-lg-0 my-3"><a class="nav-link" href="{{ url($link) }}">{{ $menu }}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="col ml-md-auto header-loginw3ls">
                <a href="#" class="button button-secondary button-small button-raised button-pill"> Book Your Class</a>
            </div>
        </nav>
    </div>
</div>