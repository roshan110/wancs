<!-- LOADER
<div id="preloader">
    <div class="loader">
		<div class="loader__bar"></div>
		<div class="loader__bar"></div>
		<div class="loader__bar"></div>
		<div class="loader__bar"></div>
		<div class="loader__bar"></div>
		<div class="loader__ball"></div>
	</div>
</div>
 end loader -->
<!-- END LOADER -->
<div class="top-bar">
    <div class="container-fluid">
        <div class="col-sm-10 col-sm-offset-2">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="left-top">
                        <p><a href="{{ url('notice') }}" style="color: white;">Notice: {{ strip_tags(str_limit($notice->details,50)) }}</a></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="right-top">
                        <div class="email-box">
                            <a href="mailto:{{ Label::ofValue('global:email') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i>{{ Label::ofValue('global:email') }}</a>
                        </div>
                        <div class="phone-box">
                            <a href="tel:{{ Label::ofValue('global:mobile') }}"><i class="fa fa-phone" aria-hidden="true"></i>{{ Label::ofValue('global:mobile') }}</a>
                        </div>
                        <div class="social-box">
                            <ul>
                                @foreach($socials as $social)
                                    <li><a href="{{ $social->link }}"><i class="fa fa-{{ $social->icon }}-square" aria-hidden="true"></i></a></li>
                                @endforeach
                            <ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<header class="header header_style_01">
    <nav class="megamenu navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('_root_') }}"><img src="{{ asset('images/logos/logo.png') }}" alt="image" width="150"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    @foreach ($main_menu as $link=>$menu)
                        @if (is_array($menu))
                            {{-- @if($menu[$link] == "Courses")
                                <li class="nav-item mx-lg-2 my-lg-0 my-3 dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">{{ $menu[$link] }}</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @foreach ($menu['sub'] as $sub_link=>$sub_menu)
                                            <a class="dropdown-item" href="{{ url('courses/'.$sub_link) }}">{{ $sub_menu }}</a>
                                        @endforeach
                                    </div>
                                </li>
                            @else --}}
                                <li class="nav-item mx-lg-2 my-lg-0 my-3">
                                    <a class="nav-link {{ Request::url() == url($link) ? 'active' : '' }}" href="{{ url($link) }}" {{-- id="navbarDropdown" role="button" data-toggle="dropdown" --}} aria-haspopup="true"
                                        aria-expanded="false">{{ $menu[$link] }}</a>
                                    {{-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @foreach ($menu['sub'] as $sub_link=>$sub_menu)
                                            <a class="dropdown-item" href="{{ url($sub_link) }}">{{ $sub_menu }}</a>
                                        @endforeach
                                    </div> --}}
                                </li>
                            {{-- @endif --}}
                        @else
                            <li class="nav-item mx-lg-2 my-lg-0 my-3"><a class="nav-link {{ Request::url() == url($link) ? 'active' : '' }}" href="{{ url($link) }}">{{ $menu }}</a></li>
                        @endif
                    @endforeach

					<li><a class="active" href="#" data-toggle="modal" data-target="#classBook">Book Your Class</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>


<div class="modal fade" id="classBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-10">
                        <h2 class="modal-title" id="exampleModalLabel">Book Your Class</h2>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <form class="row" action="{{ route('class.book') }}" name="contactform" method="post">
                    {!! csrf_field() !!}
                    <fieldset class="row-fluid">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" placeholder="Name" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <input type="tel" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <input type="text" name="address" id="address" class="form-control" value="{{ old('address') }}" placeholder="Address" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <select name="course_id" class="form-control">
                                <option value="0">Select Your Course</option>
                                <optgroup label="Nepali">
                                    @foreach($booking_courses as $course)
                                        @if($course->main_course == 'Nepali')
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Math">
                                    @foreach($booking_courses as $course)
                                        @if($course->main_course == 'Math')
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Cultural">
                                    @foreach($booking_courses as $course)
                                        @if($course->main_course == 'Cultural')
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ route('captcha') }}" alt="Captcha">
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control text {{ $errors->has('captcha') ? ' is-invalid' : '' }}" id="captcha" name="captcha" placeholder="Enter Captcha" required="">
                                    @if ($errors->has('captcha'))
                                    <span class="help-block text-danger"><strong>{{ $errors->first('captcha') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                            <button type="submit" class="btn">Book</button>
                            <button class="btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>