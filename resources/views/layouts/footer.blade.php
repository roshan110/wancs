<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="widget clearfix text">
                    <div class="widget-title">
                        <h3>{{ config('app.name') }}</h3>
                    </div>
                    <div class="text-justify">{!! str_limit($about->details,400) !!}</div>
                </div><!-- end clearfix -->
            </div><!-- end col -->

			<div class="col-md-2 col-sm-4 col-xs-12 col-md-offset-1">
                <div class="widget clearfix">
                    <div class="widget-title">
                        <h3>Pages</h3>
                    </div>

                    <ul class="footer-links hov">
                        @foreach ($footer_menu as $item)
                            <li><a href="{{ $item->url }}">{{ $item->title }}<span class="icon icon-arrow-right2"></span></a></li>
                        @endforeach
                    </ul><!-- end links -->
                </div><!-- end clearfix -->
            </div><!-- end col -->

            <div class="col-md-4 col-md-offset-1 col-sm-4 col-xs-12">
                <div class="footer-distributed widget clearfix">
                    <div class="widget-title">
                        <h3>Like On us Facebook</h3>
                    </div>

                    <div class="fb-page" data-href="{{ \App\Setting::ofValue('facebook_page') }}" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="{{ \App\Setting::ofValue('facebook_page') }}" class="fb-xfbml-parse-ignore">
                            <a href="{{ \App\Setting::ofValue('facebook_page') }}">
                                {{ config('app.name') }}
                            </a>
                        </blockquote>
                    </div>

                </div><!-- end clearfix -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</footer><!-- end footer -->

<div class="copyrights">
    <div class="container">
        <div class="footer-distributed">
            <div class="footer-left">
                <p class="footer-company-name ">All Rights Reserved. &copy; 2019 <a href="{{ route('_root_') }}">{{ config('app.name') }}</a> Design By :
				<a href="https://w3web.co.uk/" target="_blank">W3 Web Technology</a></p>
            </div>
        </div>
    </div><!-- end container -->
</div><!-- end copyrights -->

<a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>