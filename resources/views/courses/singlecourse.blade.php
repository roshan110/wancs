@extends('layouts.app')
@section('title') {{ $course->title }}
@endsection

@section('content')

    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $course->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="about" class="section wb">
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="message-box text-center">
                                    <h2>{{ $course->title }}</h2>
                                    <div>
                                        <img class="text-center" src="{{ $course->image() }}" alt="{{ $course->title }}" width="100%" height="400">
                                    </div><br>
                                    <div class="text-justify"> 
                                    {!! $course->details !!}
                                </div>
                                </div><!-- end messagebox -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end container -->
                </div>
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
@endsection