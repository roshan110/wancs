@extends('layouts.app')
@section('title') {{ $page->title }}
@endsection

@section('content')
    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $page->title }}</h2>
                        <ul class="page-title-link">
                            <li><a href="{{ route('_root_') }}">Home</a></li>
                            <li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="portfolio" class="section wb">
                    <div>
                        <div class="section-title text-center">
                            <h3>{{ $page->title }}</h3>
                            {{-- <p class="lead">We build professional website templates, web design projects, material designs and UI kits. <br>Some of our awesome completed projects in below.</p> --}}
                        </div><!-- end title -->
                    </div><!-- end container -->

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="portfolio-filter text-center">
                                    <ul>
                                        <li><a class="btn btn-dark btn-radius btn-brd active" href="#" data-filter="*"><span class="oi hidden-xs" data-glyph="grid-three-up"></span> All</a></li>
                                        <li><a class="btn btn-dark btn-radius btn-brd" data-toggle="tooltip" data-placement="top" href="#" data-filter=".nepali">Nepali</a></li>
                                        <li><a class="btn btn-dark btn-radius btn-brd" href="#" data-toggle="tooltip" data-placement="top" data-filter=".math">Math</a></li>
                                        <li><a class="btn btn-dark btn-radius btn-brd" href="#" data-toggle="tooltip" data-placement="top" data-filter=".cultural">Cultural</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>

                        <hr class="invis">

                        <div id="da-thumbs" class="da-thumbs portfolio">
                            @foreach($courses as $course)
                                <div class="post-media pitem item-w1 item-h1 {{ str_slug($course->main_course) }}">
                                    <a href="{{ $course->image() }}" data-rel="prettyPhoto[gal]">
                                        <img src="{{ $course->image() }}" alt="" class="img-responsive">
                                        <div>
                                            <h3>{{ $course->title }} <small>{{ $course->main_course }}</small></h3>
                                            <i class="flaticon-unlink"></i>
                                        </div>
                                    </a><br>
                                    <h3><a href="{{ route('single.course', [str_slug($course->main_course), $course->title]) }}">{{ $course->title }}</a></h3>
                                </div>
                            @endforeach
                        </div><!-- end portfolio -->
                    </div><!-- end container -->
                </div>
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
@endsection

