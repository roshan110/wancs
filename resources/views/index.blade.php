@extends('layouts.app')

@section('title','Home')

@section('content')
    <div class="slider-area">
        <div class="slider-wrapper owl-carousel">
            @foreach ($slides as $slide)
                <div class="slider-item home-one-slider-otem slider-item-four slider-bg-one" style="background-image:url({{ $slide->image() }});">
                    {{-- {{ asset('uploads/slide/' . $slide->image) }} --}}
                    <div class="container">
                        <div class="row">
                            <div class="slider-content-area">
                                <div class="slide-text">
                                    <h1 class="homepage-three-title">{{ $slide->title }}</h1>

                                    <!-- <div class="slider-content-btn">
                                        <a class="button btn btn-light btn-radius btn-brd" href="#">Read More</a>
                                        <a class="button btn btn-light btn-radius btn-brd" href="#">Contact</a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="about" class="section wb">
                    <div >

                        <div class="row">
                            <div class="col-md-12">
                                <div class="message-box text-justify">
                                    {{-- <h4>About Us</h4> --}}
                                    <h2 class="text-primary">{{ Label::ofValue('global:welcome_text') }}</h2>
                                    {!! $about->details !!}

                                    <a href="{{ url('about') }}" class="btn btn-light btn-radius btn-brd grd1">Learn More</a>
                                </div><!-- end messagebox -->
                            </div><!-- end col -->
                        </div><!-- end row -->

                    </div><!-- end container -->
                </div><!-- end section -->

                <div class="parallax section parallax-off" data-stellar-background-ratio="0.9" style="background-image:url('{{asset('images/parallax_04.jpg')}}');">
                    <div>
                        <div class="row text-center stat-wrap">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <span data-scroll class="global-radius icon_wrap effect-1"><i class="flaticon-idea"></i></span>
                                <p class="stat_count">{{ \App\Label::ofValue('global:counter1_value') }}</p>
                                <h3>{{ \App\Label::ofValue('global:counter1') }}</h3>
                            </div><!-- end col -->

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <span data-scroll class="global-radius icon_wrap effect-1"><i class="flaticon-briefcase"></i></span>
                                <p class="stat_count">{{ \App\Label::ofValue('global:counter2_value') }}</p>
                                <h3>{{ \App\Label::ofValue('global:counter2') }}</h3>
                            </div><!-- end col -->

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <span data-scroll class="global-radius icon_wrap effect-1"><i class="flaticon-happy"></i></span>
                                <p class="stat_count">{{ \App\Label::ofValue('global:counter3_value') }}</p>
                                <h3>{{ \App\Label::ofValue('global:counter3') }}</h3>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end container -->
                </div><!-- end section -->

                <div id="services" class="parallax section-copy lb">
                    <div>
                        <div class="section-title text-left">
                            <h3>{{ \App\Label::ofValue('global:course_section_title') }}</h3>
                            <p class="lead">{{ \App\Label::ofValue('global:course_section_detail') }}</p>
                        </div><!-- end title -->

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="service-widget">
                                    <div class="post-media wow fadeIn">
                                        <a href="images/service_01.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                        <img src="images/service_01.jpg" alt="" class="img-responsive img-rounded">
                                    </div>
                                    <div class="service-dit">
                                        <h3><a href="{{ route('courses') }}">{{ $nepali_course->title }}</a></h3>
                                        {!! $nepali_course->details !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end service -->

                            <div class="col-sm-4">
                                <div class="service-widget">
                                    <div class="post-media wow fadeIn">
                                        <a href="images/service_02.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                        <img src="images/service_02.jpg" alt="" class="img-responsive img-rounded">
                                    </div>
                                    <div class="service-dit">
                                        <h3><a href="{{ route('courses') }}">{{ $math_course->title }}</a></h3>
                                        {!! $math_course->details !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end service -->

                            <div class="col-sm-4">
                                <div class="service-widget">
                                    <div class="post-media wow fadeIn">
                                        <a href="images/service_03.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                        <img src="images/service_03.jpg" alt="" class="img-responsive img-rounded">
                                    </div>
                                    <div class="service-dit">
                                        <h3><a href="{{ route('courses') }}">{{ $cultural_course->title }}</a></h3>
                                        {!! $cultural_course->details !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end service -->
                        </div><!-- end row -->
                    </div><!-- end container -->
                </div><!-- end section -->

                <div id="upcomingevent" class="section-copy wb" style="padding: 0 0 60px;">
                    <div >

                        <div class="row">
                            <div class="col-md-8">
                                <div class="message-box text-justify">
                                    <div class="section-title text-left">
                                    <h3>Upcoming Event</h3>
                                    </div>
                                    <div class="service-widget">
                                        @if($event['image'] !=null)
                                        <div class="post-media wow fadeIn">
                                            <a href="{{ asset('uploads/events/'.$event->image) }}" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                            <img src="{{ asset('uploads/events/'.$event->image) }}" alt="" class="img-responsive img-rounded">
                                        </div>
                                        @endif
                                        <div class="service-dit">
                                            <h3><a href="{{ route('single.event',$event->id) }}">{{$event['title']}}</a></h3>
                                        </div>
                                            <div class="countdown-box" id="countdown">
                                                            <span>
                                                                <span id="day" class="timer">00</span>
                                                                <small>Days</small>
                                                            </span>
                                                            <span>
                                                                <span id="hour" class="timer">00</span>
                                                                <small>Hours</small>
                                                            </span>
                                                            <span>
                                                                <span id="min" class="timer">00</span>
                                                                <small>Minutes</small>
                                                            </span>
                                                            <span>
                                                                <span id="sec" class="timer">00</span>
                                                                <small>Second</small>
                                                            </span>
                                            </div>
                                    </div>
                                    <a href="{{ route('single.event',$event->id) }}" class="btn btn-light btn-radius btn-brd grd1">Learn More</a>
                                </div><!-- end messagebox -->
                            </div><!-- end col -->
                        </div><!-- end row -->

                    </div><!-- end container -->
                </div><!-- end section -->
            </div>

            <div class="col-md-3">
                <div class="section wb text-justify">
                    <h2>Notice</h2>
                    {{-- <h4>{{ $notice->title }}</h4> --}}
                    {!! str_limit($notice->details, 250) !!}<br>

                    <a href="{{ url('notice') }}" class="btn btn-light btn-radius btn-brd grd1">Read more</a>
                </div>

                @include('layouts.sidebar')

                {{--<div class="section wb text-justify">--}}
                    {{--<h2>Upcomming Event</h2>--}}

                {{--</div>--}}
            </div>
        </div>
    </div>

    @if(count($reviews) > 0)
    <div id="testimonials" class="parallax section db parallax-off" style="background-image:url('{{asset('images/parallax_04.jpg')}}');">
        <div class="container mt-0">
            <div class="row">
                <div class="section-title text-center">
                    <h3>{{ \App\Label::ofValue('global:testimonial_title') }}</h3>
                    <p class="lead">{{ \App\Label::ofValue('global:testimonial_detail') }}</p>
                </div><!-- end title -->
            </div>


            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="testi-carousel owl-carousel owl-theme">
                        @foreach($reviews as $review)
                            <div class="testimonial clearfix">
                                <div class="desc">
                                    <h3><i class="fa fa-quote-left"></i>{{ $review->title }}</h3>
                                    {!! $review->details !!}
                                    <h3>{{ $review->name }} <small>- Manager of Racer</small></h3>
                                </div>
                                <!-- end testi-meta -->
                            </div>
                            <!-- end testimonial -->
                        @endforeach
                    </div><!-- end carousel -->
                </div><!-- end col -->
            </div><!-- end row -->

        </div><!-- end container -->
    </div><!-- end section -->
    @endif
@endsection

@section('script')
    <script src="{{ asset('vendors/countdown/jquery.countdown.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#countdown').countdown('{{ $event->event_date->format('Y/m/d') }}', function(event) {
                $(this).find('span#day').html(event.strftime('%D'));
                $(this).find('span#hour').html(event.strftime('%H'));
                $(this).find('span#min').html(event.strftime('%M'));
                $(this).find('span#sec').html(event.strftime('%S'));
            });
        })

    </script>
@endsection