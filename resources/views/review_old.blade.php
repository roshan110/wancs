@extends('layouts.app') 
@section('title') {{ $page->m_title }}
@endsection
 
@section('content')

<div class="features py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="text-center mb-lg-5 mb-4">
            <h3 class="tittle mb-2">{{ $page->title }}</h3>
        </div>
        <div class="row features-row">
            <div class="col-md-7">

                @foreach ($reviews as $review)

                <div class="review">
                    <h4>{{ $review->name }}</h4>
                    {!! $review->details !!}
                </div>
                <hr> @endforeach

            </div>
            <div class="col-md-4 offset-md-1">
                <h3>Write Review</h3>
                <hr>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <hr>
                @endif @if (Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>
                        {{ Session::get('success') }}
                    </p>
                </div>
                <hr>
                @endif


                <form action="{{ route('review') }}" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="">Photo (optional)</label>
                        <input type="file" name="image">
                    </div>

                    <div class="form-group">
                        <label for="">Review</label>
                        <textarea name="review" id="review" class="form-control" value="{{ old('review') }}"></textarea>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-2">
                            <img src="{{ route('captcha') }}" alt="Captcha">
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control text {{ $errors->has('captcha') ? ' is-invalid' : '' }}" id="captcha" name="captcha" placeholder="Enter Captcha" required="">
                            @if ($errors->has('captcha'))
                            <span class="help-block text-danger"><strong>{{ $errors->first('captcha') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    
                </form>
            </div>
        </div>

    </div>
</div>
@endsection