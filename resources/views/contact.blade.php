@extends('layouts.app')

@section('title') {{ $page->title }} @endsection

@section('content')
	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>{{ $page->title }}</h2>
						<ul class="page-title-link">
							<li><a href="{{ route('_root_') }}">Home</a></li>
							<li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div id="contact" class="section wb">
        <div class="container">
            <div class="section-title text-center">
                <h3>Get in touch</h3>
                <p class="lead">Let us give you more details about the special offer website you want us. Please fill out the form below. <br>We have million of website owners who happy to work with us!</p>
            </div><!-- end title -->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                	@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						<hr> 
					@endif 

					@if (Session::has('success'))
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p>
								{{ Session::get('success') }}
							</p>
						</div>
						<hr> 
					@endif

                    <div class="contact_form">
                        <div id="message"></div>
                        <form class="row" action="{{ route('contactmail') }}" name="contactform" method="post">
                        	{!! csrf_field() !!}
                            <fieldset class="row-fluid">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" placeholder="Your Name" required>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Your Email" required>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="tel" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" placeholder="Your Phone" required>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="form-control" name="message" id="message" rows="6" value="{{ old('message') }}" placeholder="Message" required></textarea>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
			              				<div class="col-md-2">
			                  				<img src="{{ route('captcha') }}" alt="Captcha">
			              				</div>
			              				<div class="col-md-10">
			             					<input type="text" class="form-control text {{ $errors->has('captcha') ? ' is-invalid' : '' }}" id="captcha" name="captcha" placeholder="Enter Captcha" required="">
			                                @if ($errors->has('captcha'))
			                                <span class="help-block text-danger"><strong>{{ $errors->first('captcha') }}</strong></span>
			                                @endif
			              				</div>
			            			</div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                                    <button type="submit" class="btn btn-light btn-radius btn-brd grd1 btn-block">Submit</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
			
			<div class="row">
				<div class="col-md-offset-1 col-sm-10 col-md-10 col-sm-offset-1 pd-add">
					<div class="address-item">
						<div class="address-icon">
							<i class="icon icon-location2"></i>
						</div>
						<h3>Address</h3>
						<p>{{ \App\Label::ofValue('global:address') }}</p>
					</div>
					<div class="address-item">
						<div class="address-icon">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<h3>Email Us</h3>
						<p><a href="mailto:{{ \App\Label::ofValue('global:email') }}">{{ \App\Label::ofValue('global:email') }}</a></p>
					</div>
					<div class="address-item">
						<div class="address-icon">
							<i class="icon icon-headphones"></i>
						</div>
						<h3>Call Us</h3>
						<p><a href="tel:{{ \App\Label::ofValue('global:mobile') }}">{{ \App\Label::ofValue('global:mobile') }}</a></p>
					</div>
				</div>
			</div><!-- end row -->
			
        </div><!-- end container -->
    </div>
@endsection