@extends('layouts.app')
@section('title') {{ $page->title }}
@endsection

@section('content')
    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $page->title }}</h2>
                        <ul class="page-title-link">
                            <li><a href="{{ route('_root_') }}">Home</a></li>
                            <li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="about" class="section wb">
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="message-box text-justify">
                                    {{-- <h4>About Us</h4>
                                    <h2>Welcome to GoodWEB Solutions</h2> --}}
                                    @if($doc_auth)
                                        @if(count($documents) > 0)
                                            <ul>
                                                @foreach($documents as $document)
                                                    <div class="col-md-6">
                                                        <li><a href="{{ asset('uploads/document/' . $document->document ) }}" target="_blank">{{ $document->title }}</a></li>
                                                    </div>
                                                @endforeach
                                            </ul>
                                        @else
                                            <div class="card border-0">
                                                <div class="card-body">
                                                    No documents available.
                                                </div>
                                            </div>
                                        @endif
                                    @else

                                        <div class="card bg-light">
                                            <div class="card-header">Enter Document Password</div>
                                            <div class="card-body">

                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul class="list-unstyled">
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif


                                                <form action="{{ route('document') }}" method="post">
                                                    {!! csrf_field() !!}
                                                    <input type="password" required name="password"><br><br>
                                                    <button class="btn btn-primary">Enter</button>
                                                </form>
                                            </div>
                                        </div>

                                    @endif
                                </div><!-- end messagebox -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end container -->
                </div>
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>


    @endsection