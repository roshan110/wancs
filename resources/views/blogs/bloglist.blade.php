@extends('layouts.app')
@section('title') {{ $page->title }}
@endsection

@section('content')
    <div class="banner-area banner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner">
                        <h2>{{ $page->title }}</h2>
                        <ul class="page-title-link">
                            <li><a href="{{ route('_root_') }}">Home</a></li>
                            <li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="features" class="section wb">
                    <div>
                        <div class="row text-center">
                            @foreach($blogs as $blog)
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="service-widget">
                                        @if($blog->image)
                                            <div class="post-media wow fadeIn">
                                                <a href="{{ $blog->image() }}" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                                <img src="{{ $blog->image() }}" alt="" style="margin-bottom: 10px;" width="400" height="150">
                                            </div>
                                        @endif
                                        <h3><a href="{{ route('single.blog', $blog->slug) }}">{{ $blog->title }}</a></h3>
                                        <p class="text-justify">{{ str_limit(strip_tags($blog->details),150) }}</p>
                                    </div><!-- end service -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
@endsection