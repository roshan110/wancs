<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Course</label>
	<div class="col-sm-6">
		<select name="course" id="course" class="form-control">
			<option selected="selected">---Select Course---</option>
			<optgroup label="Nepali">
				@foreach($courses as $course)
					@if($course->main_course == 'Nepali')
						<option value="{{ $course->main_course }}">{{ $course->title }}</option>
					@endif
				@endforeach
			</optgroup>
			<optgroup label="Math">
				@foreach($courses as $course)
					@if($course->main_course == 'Math')
						<option value="{{ $course->main_course }}">{{ $course->title }}</option>
					@endif
				@endforeach
			</optgroup>
			<optgroup label="Cultural">
				@foreach($courses as $course)
					@if($course->main_course == 'Cultural')
						<option value="{{ $course->main_course }}">{{ $course->title }}</option>
					@endif
				@endforeach
			</optgroup>

		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($document) ? $document->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($document) ? $document->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="document">Document</label>
	<div class="col-sm-6">
		<input type="file" name="document" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
	</div>
</div>