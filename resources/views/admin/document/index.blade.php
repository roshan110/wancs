@extends('admin.app')

@section('title')
Documents
@endsection

@section('content')


<h3 class="page-title">Documents<a href="{{ route('documents.create') }}" class="btn btn-primary pull-right"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a></h3>


<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Title</th>
				<th>Course</th>
				<th>Document</th>
				<th>Created At</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($documents as $document)			
			<tr>
				<td>{{ $document->title }}</td>
				<td>{{$document->course}}</td>
				<td><img src="{{ asset('uploads/document/'.$document->document) }}" height="100"></td>
				<td>{{ $document->created_at->format('M d, Y') }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('documents.edit', $document->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('documents.destroy', $document->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection