@extends('admin.app') 
@section('title') Dashboard
@endsection
 
@section('content')
<h3 class="page-title">Dashboard</h3>

<div class="row">
	<div class="col-sm-12">
        <div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-users fa-fw"></span> Course Applications</h3>
                {{--
                <p class="panel-subtitle">Date: {{date('d M, y')}}</p> --}}
            </div>
            <div class="panel-body">
                @if ($applications->count() > 0)
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Course</th>
								<th>Applicant</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Created At</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($applications as $application)
                            <tr>
                                <td>{{ $application->course->main_course }} - {{ $application->course->title }}</td>
								<td>{{ $application->name }}</td>
								<td>{{ $application->email }}</td>
								<td>{{ $application->phone }}</td>
								<td>{{ $application->created_at->format('M d, Y') }}</td>
								<td><a href="{{ route('course.application.view', $application->id) }}" class="btn btn-primary btn-xs"><i class="lnr lnr-eye"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <p>Records not found</p>
                @endif

            </div>
            
            <div class="panel-footer">
                <a href="{{ route('course.application') }}" class="btn btn-primary">View all</a>
            </div>
        </div>
    </div>
</div>
@endsection