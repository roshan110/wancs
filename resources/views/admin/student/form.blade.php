<div class="form-group">
	<label class="col-sm-2 control-label" for="name">Name</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Name" name="name" value="{{ old('name', isset($student) ? $student->name : null) }}" type="text" id="name" autofocus>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="email">Email</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Email" name="email" value="{{ old('email', isset($student) ? $student->email : null) }}" type="email" id="email">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="phone">Phone</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Phone" name="phone" value="{{ old('phone', isset($student) ? $student->phone : null) }}" type="tel" id="phone">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="address">Address</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Address" name="address" value="{{ old('address', isset($student) ? $student->address : null) }}" type="text" id="address">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="parent_name">Parent's Name</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Parent's Name" name="parent_name" value="{{ old('parent_name', isset($student) ? $student->parent_name : null) }}" type="text" id="parent_name">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="parent_email">Parent's Email</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Parent's Email" name="parent_email" value="{{ old('parent_email', isset($student) ? $student->parent_email : null) }}" type="email" id="parent_email">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="parent_phone">Parent's Phone</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Parent's Phone" name="parent_phone" value="{{ old('parent_phone', isset($student) ? $student->parent_phone : null) }}" type="tel" id="parent_phone">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="parent_address">Parent's Address</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Parent's Address" name="parent_address" value="{{ old('parent_address', isset($student) ? $student->parent_address : null) }}" type="text" id="parent_address">
	</div>
</div>

