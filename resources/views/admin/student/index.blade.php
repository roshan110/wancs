@extends('admin.app')

@section('title')
Students
@endsection

@section('content')

<h3 class="page-title">Students <a href="{{ route('students.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">		
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Parent's Name</th>
				<th>Parent's Email</th>
				<th>Parent's Phone</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($students as $student)			
			<tr>
				<td>{{ $student->name }}</td>
				<td>{{ $student->email }}</td>
				<td>{{ $student->phone }}</td>
				<td>{{ $student->parent_name }}</td>
				<td>{{ $student->parent_email ? $student->parent_email : '-' }}</td>
				<td>{{ $student->parent_phone ? $student->parent_phone : '-' }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('students.edit', $student->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('students.destroy', $student->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

