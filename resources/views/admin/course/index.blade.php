@extends('admin.app')

@section('title')
Courses
@endsection

@section('content')

<h3 class="page-title">Courses <a href="{{ route('courses.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Main Course</th>
				<th>Title</th>
				<th>Created At</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="sortable">
			@foreach ($courses as $course)

			<tr id="{{ $course->id }}">
				<td>{{ $course->main_course }}</td>
				<td>{{ $course->title }}</td>
				<td>{{ $course->created_at->format('M d, Y') }}</td>
				<td class="text-right">
					<a class="btn btn-primary btn-sm" href="{{ route('courses.edit', $course->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;">
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('courses.destroy', $course->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection
