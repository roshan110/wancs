<div class="form-group">
	<label class="col-sm-2 control-label" for="main_course">Course Title</label>
	<div class="col-sm-3">
		<select class="form-control" name="main_course" id="main_course">
			<option value="Nepali" {{ (isset($course) && $course->main_course == "Nepali") ? 'selected' : "Nepali"  }}>Nepali</option>
			<option value="Math" {{ (isset($course) && $course->main_course == "Math") ? 'selected' : "Math"  }}>Math</option>
			<option value="Cultural" {{ (isset($course) && $course->main_course == "Cultural") ? 'selected' : "Cultural"  }}>Cultural</option>
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($course) ? $course->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($course) ? $course->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="duration">Duration</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Duration" name="duration" value="{{ old('duration', isset($course) ? $course->duration : null) }}" type="text" id="duration">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="price">Price</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Price" name="price" value="{{ old('price', isset($course) ? $course->price : null) }}" type="text" id="price">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image</label>
	<div class="col-sm-10">
		<input type="file" name="image" id="image">
	</div>
			</div>