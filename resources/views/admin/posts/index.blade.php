@extends('admin.app')

@section('title')
News & Updates
@endsection

@section('content')

<h3 class="page-title">News & Updates <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">		
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Image</th>
				<th>Title</th>
				<th>Status</th>
				<th>Created At</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="sortable">
			@foreach ($posts as $post)			

			<tr id="{{ $post->id }}">
				<td><img src="{{ $post->image() }}" width="100"></td>
				<td>{{ $post->title }}</td>
				<td>
					@if ($post->status)
						<a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click to hide" href="{{ route('posts.showhide', $post->id) }}">
							<i class="lnr lnr-checkmark-circle"></i>
							Active
						</a>
					@else
						<a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click to show" href="{{ route('posts.showhide', $post->id) }}">
							<i class="lnr lnr-cross-circle"></i>
							Draft
						</a>
					@endif 
				</td>
				<td>{{ $post->created_at->format('M d, Y') }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('posts.edit', $post->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('posts.destroy', $post->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

