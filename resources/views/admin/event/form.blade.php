<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($event) ? $event->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-8">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($event) ? $event->details : null) }}</textarea>
	</div>
</div>



<div class="form-group">
	<label class="col-sm-2 control-label" for="event_place">Event Place</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Event Place" name="event_place" value="{{ old('event_place', isset($event) ? $event->event_place : null) }}" type="text" id="event_place">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="event_date">Event Date</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control datepicker" placeholder="Event Date" name="event_date" value="{{ old('event_date', isset($event) ? $event->event_date : null) }}" type="text" id="event_date">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="end_date">End Date (Optional)</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control datepicker" placeholder="End Date" name="end_date" value="{{ old('end_date', isset($event) ? $event->end_date : null) }}" type="text" id="end_date">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image</label>
	<div class="col-sm-10">
		@if(isset($event) && $event->image)
			<img src="{{asset('/uploads/events/'.$event->image)}}">
			<a href="{{route('events.photodelete',$event->id)}}" class="btn btn-danger">Delete this image</a>
			<br>
		@endif
		<input type="file" name="image" id="image" class="form-control">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="show_in_countdown">Show in Countdown</label>
	<div class="col-sm-10">
		<input type="checkbox" name="show_in_countdown" class="show_in_countdown" id="show_in_countdown" value="1" {{ old('show_in_countdown', isset($event) ? $event->show_in_countdown : null) == 1 ? 'checked="checked"' : null }}>
	</div>
</div>