@extends('admin.app')

@section('title')
Events
@endsection

@section('content')

	<h3 class="page-title">Events <a href="{{ route('events.create') }}" class="btn btn-primary pull-right"><i class="lnr lnr-plus-circle"></i> <span>Add New</span></a></h3>



<div class="content-table">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Title</th>
				<th>Place</th>
				<th>Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($events as $event)			
			<tr>
				<td>{{ $event->title }}</td>
				<td>{{ $event->event_place }}</td>
				<td>{{ $event->event_date }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('events.edit', $event->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('events.destroy', $event->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection