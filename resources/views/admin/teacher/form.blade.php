<div class="form-group">
	<label class="col-sm-2 control-label" for="name">Name</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Name" name="name" value="{{ old('name', isset($teacher) ? $teacher->name : null) }}" type="text" id="name" autofocus>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="email">Email</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Email" name="email" value="{{ old('email', isset($teacher) ? $teacher->email : null) }}" type="email" id="email">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="phone">Phone</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Phone" name="phone" value="{{ old('phone', isset($teacher) ? $teacher->phone : null) }}" type="tel" id="phone">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" name="image" type="file" id="image">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="address">Address</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Address" name="address" value="{{ old('address', isset($teacher) ? $teacher->address : null) }}" type="text" id="address">
	</div>
</div>


