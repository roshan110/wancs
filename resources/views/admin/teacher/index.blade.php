@extends('admin.app')

@section('title')
Teachers
@endsection

@section('content')

<h3 class="page-title">Teachers <a href="{{ route('teachers.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">		
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Address</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($teachers as $teacher)			
			<tr>
				<td>{{ $teacher->name }}</td>
				<td>{{ $teacher->email }}</td>
				<td>{{ $teacher->phone }}</td>
				<td>{{ $teacher->address }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('teachers.edit', $teacher->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('teachers.destroy', $teacher->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

