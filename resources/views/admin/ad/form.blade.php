<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($ad) ? $ad->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="url">Link</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Link" name="url" value="{{ old('url', isset($ad) ? $ad->url : null) }}" type="text" id="url">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="icon">Type</label>
	<div class="col-sm-9">
		<div class="row">
			@foreach (config('ads') as $key=>$type_ad)
			<label class="col-sm-6" title="{{ $type_ad['helper_txt'] }}"><input type="checkbox" name="type[]" value="{{ $key }}" {{ isset($ad) && in_array($key, $ad->type)? "checked='checked'" : null}}>{{ $type_ad['title'] }} [{{ $type_ad['width'] }},{{ $type_ad['height'] }}]</label>
			@endforeach				
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image</label>
	<div class="col-sm-10">
		<input type="file" name="image" id="image">
	</div>
</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-2">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="status" id="status" value="1" {{ old('status', isset($ad) ? $ad->status : 1) == 1 ? 'checked=checked' : null  }}> Active
			</label>
		</div>
	</div>
</div>
