@extends('admin.app')

@section('title')
Ads
@endsection

@section('content')

<h3 class="page-title">Ads <a href="{{ route('ads.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<form action="" class="form-horizontal">
			<div class="form-group">
				<label for="" class="col-sm-offset-6 col-sm-3 control-label">filter</label>
				<div class="col-sm-3">
					<select name="filter" id="ads_filter" class="form-control" onchange="this.form.submit()">
						<option value="all">All</option>
						@foreach(config('ads') as $key=>$ad)
						<option value="{{ $key }}" {{ request()->filter && request()->filter == $key ? 'selected="selected"':null }}>{{ $ad['title'] }}</option>
						@endforeach
					</select>	
				</div>
			</div>
		</form>

		<hr>

		<table class="table table-hover">
		<thead>
			<tr>
				<th>Image</th>
				<th>Title</th>
				<th>Type</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($ads as $ad)			
			<tr>
				<td><img src="{{ asset('uploads/ads/' . $ad->image) }}" width="100" alt=""></td>
				<td>{{ $ad->title }}</td>
				<td>{{ $ad->f_type }}</td>
				<td>
					@if ($ad->status)
					<a class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click to hide" href="{{ route('ads.showhide', $ad->id) }}"><i class="lnr lnr-checkmark-circle"></i></a>					@else
					<a class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Click to show" href="{{ route('ads.showhide', $ad->id) }}"><i class="lnr lnr-cross-circle"></i></a>					@endif [{{ $ad->status ? 'Active' : 'Draft' }}]
				</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('ads.edit', $ad->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('ads.destroy', $ad->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

