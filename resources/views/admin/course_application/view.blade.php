@extends('admin.app')

@section('title')
Course Application
@endsection

@section('content')
<h3 class="page-title">Course Application of <b>{{ $application->name }}</b> <a href="{{ route('course.application') }}" class="btn btn-primary pull-right"><span><i class="lnr lnr-arrow-left"></i> Back</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<div class="table">
			<div>
				<label>Name:</label>
				<p>{{ $application->name }}</p>
			</div>

			<div>
				<label>Email:</label>
				<p>{{ $application->email }}</p>
			</div>

			<div>
				<label>Phone No:</label>
				<p>{{ $application->phone }}</p>
			</div>

			<div>
				<label>Address:</label>
				<p>{{ $application->address }}</p>
			</div>

			<div>
				<label>Applied Course:</label>
				<p>{{ $application->course->main_course }} - {{ $application->course->title }}</p>
			</div>
		</div>
	</div>
</div>
@endsection