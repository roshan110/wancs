@extends('admin.app')

@section('title')
Course Applications
@endsection

@section('content')

<h3 class="page-title">Course Applications</h3>

<div class="panel">
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Course</th>
					<th>Applicant</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Created At</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($applications as $application)				
				<tr>
					<td>{{ $application->course->main_course }} - {{ $application->course->title }}</td>
					<td>{{ $application->name }}</td>
					<td>{{ $application->email }}</td>
					<td>{{ $application->phone }}</td>
					<td>{{ $application->created_at->format('M d, Y') }}</td>
					<td><a href="{{ route('course.application.view', $application->id) }}" class="btn btn-primary btn-xs"><i class="lnr lnr-eye"></i></a></td>
				</tr>			
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="panel-footer">{{ $applications->links() }}</div>
</div>
@endsection