@extends('admin.app') 
@section('title') Blogs
@endsection
 
@section('content')

<h3 class="page-title">Blogs <a href="{{ route('blogs.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Created At</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="sortable">
				@php $i=1; 
				@endphp 
				@foreach ($blogs as $page)
				<tr id="{{ $page->id }}">
					<td>{{$i}}</td>
					<td>{{ $page->title }}</td>
					<td>{{ $page->created_at->format('M d, Y') }}</td>
					<td class="text-right">

						<a class="btn btn-primary btn-sm" href="{{ route('blogs.edit', $page->id) }}"><i class="fa fa-edit"></i></a>

						<div class="pull-right" style="margin-left: 10px;">
							<form onsubmit="return confirm('Are you sure?')" action="{{ route('blogs.destroy', $page->id) }}" method="post">
								{{ method_field('DELETE') }} {{ csrf_field() }}
								<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
							</form>
						</div>
					</td>
				</tr>
				@php $i++; 
@endphp @endforeach
			</tbody>
		</table>
	</div>
	<div class="panel-footer">{{ $blogs->links() }}</div>
</div>
@endsection