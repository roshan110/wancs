@component('mail::message')

#Class Booking

Class Booking made by **{{ $request->name }}** and details are following:

**Name:** {{$request->name}}   
**Email:** {{$request->email}}     
**Phone:** {{$request->phone}}   
**Address:** {{$request->address}}   

**Course:**
{{$course->main_course}} - {{$course->title}}  



Thanks,<br>
{{ config('app.name') }}
@endcomponent
