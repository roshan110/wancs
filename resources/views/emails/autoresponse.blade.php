@component('mail::message')

Dear {{ $request->name }},
<br>

## Thank you for contacting {{ config('app.name') }}.

You should expect our response within 24 hours (Working day).

In case of emergency, Please feel free to call us.

{{ \App\Label::ofValue('global:mobile') }}
<br>
<br>
Sincerely,

Support Team <br>
{{ config('app.name') }}<br>
[{{ config('app.url') }}]({{ config('app.url') }})
@endcomponent

