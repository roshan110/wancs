@component('mail::message')

#Contact Mail

Contact Mail received from **{{ $data->name }}** and details are following:

**Name:** {{$data->name}}   
**Email:** {{$data->email}}     
**Phone:** {{$data->phone}}   

**Message:**
{{$data->message}}  



Thanks,<br>
{{ config('app.name') }}
@endcomponent
