@extends('layouts.app')
@section('title') {{ $post->title }}
@endsection

@section('content')

<div class="features py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="col-md-8 offset-md-2">
            <div class="text-center mb-lg-5 mb-4">
                <h3 class="tittle mb-2">{{ $post->title }}</h3>
            </div>
            <div class="row features-row">
                <div class="col-lg-12 mt-5">
                    <div class="news_box text-center">
                        <img src="{{ $post->image() }}" width="800" height="400">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="news_box">
                        <div class="event_txt">
                            <h3>{{ $post->title }}</h3>
                            <p class="text-justify">{{ strip_tags($post->details) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection