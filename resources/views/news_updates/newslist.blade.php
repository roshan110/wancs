@extends('layouts.app')
@section('title') {{ $page->m_title }}
@endsection

@section('content')

<div class="features py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="col-md-12">
            <div class="text-center mb-lg-5 mb-4">
                <h3 class="tittle mb-2">{{ $page->title }}</h3>
            </div>
            <div class="row features-row">
                <div class="col-lg-12 mt-5">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-4">
                                <div class="news_box">
                                    <img src="{{ $post->image() }}" width="400" height="200">
                                    <div class="event_txt">
                                        <h3><a href="{{ route('singlenews', $post->slug) }}">{{ $post->title }}</a></h3>
                                        <p class="text-justify">{{ str_limit(strip_tags($post->details),150) }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection