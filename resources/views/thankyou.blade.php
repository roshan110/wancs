@extends('layouts.app') 
@section('title') Thank You
@endsection
 
@section('content')
    <div id="about" class="section wb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="message-box text-center">
                        <h2>Thank You</h2>
                        <p>Your request has been posted successfully. We will respond you shortly after your request is reviewed.</p>
                    </div><!-- end messagebox -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div>
@endsection