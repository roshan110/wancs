@extends('layouts.app')
@section('title') {{ $page->m_title }}
@endsection

@section('content')

<div class="page-header py-5">
    <div class="text-center">
        <h1>{{ $page->title }}</h1>
    </div>
</div>

<div class="features">
    <div class="container py-xl-5 py-lg-3">
        <div class="row">

            <div class="col-md-7">
                <div class="row features-row">
                    <div class="col-lg-12 mt-5">
                        <div class="row">
                            @if($videos)
                                @foreach ($videos as $video)
                                    <div class="col-md-6">
                                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{ $video->video_id }}" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 offset-md-1">
                @include('layouts.sidebar')
            </div>

        </div>


    </div>
</div>
@endsection