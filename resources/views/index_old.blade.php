@extends('layouts.app')
@section('title') {{ $page->m_title }}
@endsection

@section('content')

<div class="homeslide owl-carousel owl-theme">

    @foreach ($slides as $slide)
    <div class="item">
        <img src="{{ asset('uploads/slide/' . $slide->image) }}" height="750" alt="{{ $slide->title }}">
        {{-- <div class="banner-text-agile">
            <div class="container">
            kml  <div class="banner-w3lstexts text-white text-center">
                    <h3>{{ $slide->title }}</h3>
                    <h4 class="text-uppercase mt-md-3 mt-2 mb-md-4 mb-3">{{ strip_tags($slide->details) }}</h4>
                    @if ($slide->link)
                        <a href="{{ $slide->link }}" class="button button-secondary button-raised">Learn More</a>
                    @endif
                </div>
            </div>
        </div> --}}
    </div>
    @endforeach

</div>


<div class="welcome">
    <div class="container">
        <div class="middle-w3l">
            <div class="row">

                <div class="col-md-{{ $notice ? 8:12 }} left-build-wthree py-5 px-sm-5 px-4">
                    <h4>{{ Label::ofValue('global:welcome_text') }}</h4>
                    <hr>
                    {{-- <h6 class="mt-3 mb-xl-5 mb-4">{{ Label::ofValue('home:welcome_sub') }}</h6> --}}
                    {!! $page->details !!}
                    <a href="{{ url('about') }}" class="button button-primary button-pill button-riase mt-2 button-small">Read More </a>

                </div>

                @if ($notice)
                <div class="col-md-4 py-5 px-sm-5 px-4">
                    <div class="notice-board text-center">
                        <h3>Notice</h3>
                        <hr>
                        <h4>{{ $notice->title }}</h4>

                        <p>{{ str_limit(strip_tags($notice->details), 200) }}</p>
                        <a href="{{ $notice->url }}" class="button button-secondary button-raised button-small mt-3">Read more</a>
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>

{{-- <div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="col-md-12">
                    <h2>Upcomming Events</h2>
                    <hr>
                    @if($event)
                        <div class="event_box box p0">
                            <div class="event-img" style="backgroud-image:url('{{ asset('uploads/events/'.$event->image) }}')"></div>
                            <div class="event-txt">
                                <h3><a href="#">{{ $event->title }}</a></h3>
                                <h4>Program Start in</h4>

                                <div class="countdown_event" id="countdown">
                                    <span>
                                        <span class="days">00</span>
                                        <small>Days</small>
                                    </span>
                                    <span>
                                        <span class="hours">00</span>
                                        <small>Hours</small>
                                    </span>
                                    <span>
                                        <span class="minutes">00</span>
                                        <small>Minutes</small>
                                    </span>
                                    <span>
                                        <span class="seconds">00</span>
                                        <small>Second</small>
                                    </span>
                                </div>

                                <a href="#" class="button button-primary button-rounded button-small">View Details</a>
                            </div>
                        </div>
                    @endif
                </div><br>

                <div class="col-md-12 mt-5">
                    <h2>News & Updates</h2>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="news_box first_post">
                                <img src="{{ $first_post->image('400x300') }}" class="img-fluid">
                                <div class="event_txt">
                                    <h3><a href="{{ route('singlenews', $first_post->slug) }}">{{ $first_post->title }}</a></h3>
                                    <p class="text-justify">{{ str_limit(strip_tags($first_post->details),150) }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            @foreach($posts as $post)
                                <div class="news_box">
                                    <img src="{{ $post->image('400x300') }}" alt="" width="100">
                                    <div class="event_txt">
                                        <h3><a href="{{ route('singlenews', $post->slug) }}">{{ $post->title }}</a></h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <div class="ml-5">
                    @include('layouts.sidebar')
                </div>
            </div>
        </div>
    </div>
</div> --}}


<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-10">
                        <h2>Our Classes</h2>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('courses') }}" class="button button-secondary button-small button-raised button-pill"> View All</a>
                    </div>
                 </div><br>
                <div class="row">
                    <div class="col-md-4">
                        <h6>Nepali</h6><br>
                        <div class="card">
                            <img class="card-img-top" src="{{ $nepali_course->image() }}" alt="{{ $nepali_course->title }}">
                            <div class="card-body">
                                <h6 style="text-transform:none;"><a href="{{ route('single.course',[$nepali_course->main_course, $nepali_course->title]) }}">{{ $nepali_course->title }}</a></h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <h6>Math</h6><br>
                        <div class="card">
                            <img class="card-img-top" src="{{ $math_course->image() }}" alt="{{ $math_course->title }}">
                            <div class="card-body">
                                <h6 style="text-transform:none;"><a href="{{ route('single.course',[$math_course->main_course, $math_course->title]) }}">{{ $math_course->title }}</a></h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <h6>Cultural</h6><br>
                        <div class="card">
                            <img class="card-img-top" src="{{ $cultural_course->image() }}" alt="{{ $cultural_course->title }}">
                            <div class="card-body">
                                <h6 style="text-transform:none;"><a href="{{ route('single.course',[$cultural_course->main_course, $cultural_course->title]) }}">{{ $cultural_course->title }}</a></h6>
                            </div>
                        </div>
                    </div>
                </div><br>

                <div class="row">
                    @if($album)
                        <div class="col-md-6">
                            <h2>Photo Gallery</h2>
                            <hr>
                            <div class="card">
                                <img class="card-img-top" src="{{ $album->thumb }}" alt="{{ $album->title }}">
                                <div class="card-body">
                                    <h6 style="text-transform:none;"><a href="{{ route('single.album',$album->slug) }}">{{ $album->title }}</a></h6>
                                </div>
                            </div>
                            <div class="text-center mt-3">
                                <a href="{{ url('photo-gallery') }}" class="button button-primary button-pill button-riase mt-2 button-small">View All</a>
                            </div>
                        </div>
                    @endif

                    @if($video)
                        <div class="col-md-6">
                            <h2>Video</h2>
                            <hr>
                            <div class="card">
                                <iframe width="100%" height="285" src="https://www.youtube.com/embed/{{ $video->video_id }}" frameborder="0" allowfullscreen></iframe>
                                <div class="card-body">
                                    <h6><a href="#">{{ $video->title }}</a></h6>
                                </div>
                            </div>
                            <div class="text-center mt-3">
                                <a href="{{ url('video-gallery') }}" class="button button-primary button-pill button-riase mt-2 button-small">View All</a>
                            </div>
                        </div>
                    @endif
                </div><br>
            </div>

            <div class="col-md-4">
                <div class="ml-5">
                    @include('layouts.sidebar')
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($reviews) > 0)
    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h4>Review</h4>
                </div>
                <div class="col-md-2">
                    <a href="{{ url('reviews') }}" class="button button-secondary button-small button-raised button-pill"> View All</a>
                </div>
             </div>
            <hr>
            <div class="row">
                @foreach($reviews as $review)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header"> {{ $review->title }} </div>

                            <div class="card-body">
                                {!! str_limit($review->details,200) !!}
                            </div>

                            <div class="card-footer">
                                <b>- {{ $review->name }}</b>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif

@endsection

{{-- @section('scripts')
    <script type="text/javascript">
        $(function () {
            $('#countdown').countdown('{{ $event->event_date->format('Y/m/d') }}', function (event) {
                $(this).find('span.days').html(event.strftime('%D'));
                $(this).find('span.hours').html(event.strftime('%H'));
                $(this).find('span.minutes').html(event.strftime('%M'));
                $(this).find('span.seconds').html(event.strftime('%S'));
            });
        })
    </script>
@endsection --}}