-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2019 at 06:51 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wisdomacademy`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `sidebar_order` int(11) DEFAULT NULL,
  `in_sidebar_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `title`, `type`, `image`, `url`, `status`, `sidebar_order`, `in_sidebar_order`, `created_at`, `updated_at`) VALUES
(1, 'sidebar 1', '[\"sidebar\"]', 'ad-1.jpeg', NULL, 1, NULL, NULL, '2019-08-15 01:07:17', '2019-08-15 01:07:17'),
(2, 'sidebar 2', '[\"sidebar\"]', 'ad-2.jpeg', NULL, 1, NULL, NULL, '2019-08-15 01:07:33', '2019-08-15 01:07:33'),
(3, 'sidebar 3', '[\"sidebar\"]', 'ad-3.jpeg', NULL, 1, NULL, NULL, '2019-08-15 01:12:37', '2019-08-15 01:12:37'),
(4, 'sidebar 4', '[\"sidebar\"]', 'ad-4.jpeg', NULL, 1, NULL, NULL, '2019-08-15 01:13:39', '2019-08-15 01:13:39'),
(5, 'asdf', '[\"sidebar\"]', 'ad-5.jpeg', NULL, 1, NULL, NULL, '2019-08-15 01:26:05', '2019-08-15 01:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `title`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Test', 'test', 1, '2019-08-15 02:42:55', '2019-08-15 02:42:55'),
(3, 'Speciality Bread', 'speciality-bread', 1, '2019-08-19 04:00:59', '2019-08-19 04:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci,
  `meta_keyword` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `slug`, `image`, `details`, `status`, `meta_title`, `meta_keyword`, `meta_description`, `created_at`, `updated_at`) VALUES
(2, 'Quam anim repudianda', 'quam-anim-repudianda', 'quam-anim-repudianda-2.jpeg', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, 'Voluptas minima in e', 'Voluptatem accusanti', 'Vel officia voluptat', '2019-08-15 02:19:19', '2019-08-15 02:19:19'),
(3, 'Beatae ad aute anim', 'beatae-ad-aute-anim', 'beatae-ad-aute-anim-3.jpeg', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, 'Esse expedita nostru', 'Velit vel nihil fug', 'Eu in rerum ad totam', '2019-08-15 02:19:31', '2019-08-15 02:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `class_books`
--

CREATE TABLE `class_books` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_books`
--

INSERT INTO `class_books` (`id`, `course_id`, `name`, `email`, `phone`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 'Yuri Howell', 'vanozoze@mailinator.net', '+1 (628) 431-4949', 'Maxime est non qui a', 1, '2019-08-28 23:48:51', '2019-08-28 23:48:51'),
(2, 1, 'Kathleen Fowler', 'kijyfut@mailinator.net', '+1 (845) 376-6946', 'Lorem cumque volupta', 1, '2019-08-28 23:49:35', '2019-08-28 23:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `sprite` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `sprite`) VALUES
(1, 'AF', 'Afghanistan', NULL),
(2, 'AL', 'Albania', NULL),
(3, 'DZ', 'Algeria', NULL),
(4, 'DS', 'American Samoa', NULL),
(5, 'AD', 'Andorra', NULL),
(6, 'AO', 'Angola', NULL),
(7, 'AI', 'Anguilla', NULL),
(8, 'AQ', 'Antarctica', NULL),
(9, 'AG', 'Antigua and Barbuda', NULL),
(10, 'AR', 'Argentina', NULL),
(11, 'AM', 'Armenia', NULL),
(12, 'AW', 'Aruba', NULL),
(13, 'AU', 'Australia', NULL),
(14, 'AT', 'Austria', NULL),
(15, 'AZ', 'Azerbaijan', NULL),
(16, 'BS', 'Bahamas', NULL),
(17, 'BH', 'Bahrain', NULL),
(18, 'BD', 'Bangladesh', NULL),
(19, 'BB', 'Barbados', NULL),
(20, 'BY', 'Belarus', NULL),
(21, 'BE', 'Belgium', NULL),
(22, 'BZ', 'Belize', NULL),
(23, 'BJ', 'Benin', NULL),
(24, 'BM', 'Bermuda', NULL),
(25, 'BT', 'Bhutan', NULL),
(26, 'BO', 'Bolivia', NULL),
(27, 'BA', 'Bosnia and Herzegovina', NULL),
(28, 'BW', 'Botswana', NULL),
(29, 'BV', 'Bouvet Island', NULL),
(30, 'BR', 'Brazil', NULL),
(31, 'IO', 'British Indian Ocean Territory', NULL),
(32, 'BN', 'Brunei Darussalam', NULL),
(33, 'BG', 'Bulgaria', NULL),
(34, 'BF', 'Burkina Faso', NULL),
(35, 'BI', 'Burundi', NULL),
(36, 'KH', 'Cambodia', NULL),
(37, 'CM', 'Cameroon', NULL),
(38, 'CA', 'Canada', NULL),
(39, 'CV', 'Cape Verde', NULL),
(40, 'KY', 'Cayman Islands', NULL),
(41, 'CF', 'Central African Republic', NULL),
(42, 'TD', 'Chad', NULL),
(43, 'CL', 'Chile', NULL),
(44, 'CN', 'China', NULL),
(45, 'CX', 'Christmas Island', NULL),
(46, 'CC', 'Cocos (Keeling) Islands', NULL),
(47, 'CO', 'Colombia', NULL),
(48, 'KM', 'Comoros', NULL),
(49, 'CG', 'Congo', NULL),
(50, 'CK', 'Cook Islands', NULL),
(51, 'CR', 'Costa Rica', NULL),
(52, 'HR', 'Croatia (Hrvatska)', NULL),
(53, 'CU', 'Cuba', NULL),
(54, 'CY', 'Cyprus', NULL),
(55, 'CZ', 'Czech Republic', NULL),
(56, 'DK', 'Denmark', NULL),
(57, 'DJ', 'Djibouti', NULL),
(58, 'DM', 'Dominica', NULL),
(59, 'DO', 'Dominican Republic', NULL),
(60, 'TP', 'East Timor', NULL),
(61, 'EC', 'Ecuador', NULL),
(62, 'EG', 'Egypt', NULL),
(63, 'SV', 'El Salvador', NULL),
(64, 'GQ', 'Equatorial Guinea', NULL),
(65, 'ER', 'Eritrea', NULL),
(66, 'EE', 'Estonia', NULL),
(67, 'ET', 'Ethiopia', NULL),
(68, 'FK', 'Falkland Islands (Malvinas)', NULL),
(69, 'FO', 'Faroe Islands', NULL),
(70, 'FJ', 'Fiji', NULL),
(71, 'FI', 'Finland', NULL),
(72, 'FR', 'France', NULL),
(73, 'FX', 'France, Metropolitan', NULL),
(74, 'GF', 'French Guiana', NULL),
(75, 'PF', 'French Polynesia', NULL),
(76, 'TF', 'French Southern Territories', NULL),
(77, 'GA', 'Gabon', NULL),
(78, 'GM', 'Gambia', NULL),
(79, 'GE', 'Georgia', NULL),
(80, 'DE', 'Germany', NULL),
(81, 'GH', 'Ghana', NULL),
(82, 'GI', 'Gibraltar', NULL),
(83, 'GK', 'Guernsey', NULL),
(84, 'GR', 'Greece', NULL),
(85, 'GL', 'Greenland', NULL),
(86, 'GD', 'Grenada', NULL),
(87, 'GP', 'Guadeloupe', NULL),
(88, 'GU', 'Guam', NULL),
(89, 'GT', 'Guatemala', NULL),
(90, 'GN', 'Guinea', NULL),
(91, 'GW', 'Guinea-Bissau', NULL),
(92, 'GY', 'Guyana', NULL),
(93, 'HT', 'Haiti', NULL),
(94, 'HM', 'Heard and Mc Donald Islands', NULL),
(95, 'HN', 'Honduras', NULL),
(96, 'HK', 'Hong Kong', NULL),
(97, 'HU', 'Hungary', NULL),
(98, 'IS', 'Iceland', NULL),
(99, 'IN', 'India', NULL),
(100, 'IM', 'Isle of Man', NULL),
(101, 'ID', 'Indonesia', NULL),
(102, 'IR', 'Iran (Islamic Republic of)', NULL),
(103, 'IQ', 'Iraq', NULL),
(104, 'IE', 'Ireland', NULL),
(105, 'IL', 'Israel', NULL),
(106, 'IT', 'Italy', NULL),
(107, 'CI', 'Ivory Coast', NULL),
(108, 'JE', 'Jersey', NULL),
(109, 'JM', 'Jamaica', NULL),
(110, 'JP', 'Japan', NULL),
(111, 'JO', 'Jordan', NULL),
(112, 'KZ', 'Kazakhstan', NULL),
(113, 'KE', 'Kenya', NULL),
(114, 'KI', 'Kiribati', NULL),
(115, 'KP', 'Korea, Democratic People\'s Republic of', NULL),
(116, 'KR', 'Korea, Republic of', NULL),
(117, 'XK', 'Kosovo', NULL),
(118, 'KW', 'Kuwait', NULL),
(119, 'KG', 'Kyrgyzstan', NULL),
(120, 'LA', 'Lao People\'s Democratic Republic', NULL),
(121, 'LV', 'Latvia', NULL),
(122, 'LB', 'Lebanon', NULL),
(123, 'LS', 'Lesotho', NULL),
(124, 'LR', 'Liberia', NULL),
(125, 'LY', 'Libyan Arab Jamahiriya', NULL),
(126, 'LI', 'Liechtenstein', NULL),
(127, 'LT', 'Lithuania', NULL),
(128, 'LU', 'Luxembourg', NULL),
(129, 'MO', 'Macau', NULL),
(130, 'MK', 'Macedonia', NULL),
(131, 'MG', 'Madagascar', NULL),
(132, 'MW', 'Malawi', NULL),
(133, 'MY', 'Malaysia', NULL),
(134, 'MV', 'Maldives', NULL),
(135, 'ML', 'Mali', NULL),
(136, 'MT', 'Malta', NULL),
(137, 'MH', 'Marshall Islands', NULL),
(138, 'MQ', 'Martinique', NULL),
(139, 'MR', 'Mauritania', NULL),
(140, 'MU', 'Mauritius', NULL),
(141, 'TY', 'Mayotte', NULL),
(142, 'MX', 'Mexico', NULL),
(143, 'FM', 'Micronesia, Federated States of', NULL),
(144, 'MD', 'Moldova, Republic of', NULL),
(145, 'MC', 'Monaco', NULL),
(146, 'MN', 'Mongolia', NULL),
(147, 'ME', 'Montenegro', NULL),
(148, 'MS', 'Montserrat', NULL),
(149, 'MA', 'Morocco', NULL),
(150, 'MZ', 'Mozambique', NULL),
(151, 'MM', 'Myanmar', NULL),
(152, 'NA', 'Namibia', NULL),
(153, 'NR', 'Nauru', NULL),
(154, 'NP', 'Nepal', NULL),
(155, 'NL', 'Netherlands', NULL),
(156, 'AN', 'Netherlands Antilles', NULL),
(157, 'NC', 'New Caledonia', NULL),
(158, 'NZ', 'New Zealand', NULL),
(159, 'NI', 'Nicaragua', NULL),
(160, 'NE', 'Niger', NULL),
(161, 'NG', 'Nigeria', NULL),
(162, 'NU', 'Niue', NULL),
(163, 'NF', 'Norfolk Island', NULL),
(164, 'MP', 'Northern Mariana Islands', NULL),
(165, 'NO', 'Norway', NULL),
(166, 'OM', 'Oman', NULL),
(167, 'PK', 'Pakistan', NULL),
(168, 'PW', 'Palau', NULL),
(169, 'PS', 'Palestine', NULL),
(170, 'PA', 'Panama', NULL),
(171, 'PG', 'Papua New Guinea', NULL),
(172, 'PY', 'Paraguay', NULL),
(173, 'PE', 'Peru', NULL),
(174, 'PH', 'Philippines', NULL),
(175, 'PN', 'Pitcairn', NULL),
(176, 'PL', 'Poland', NULL),
(177, 'PT', 'Portugal', NULL),
(178, 'PR', 'Puerto Rico', NULL),
(179, 'QA', 'Qatar', NULL),
(180, 'RE', 'Reunion', NULL),
(181, 'RO', 'Romania', NULL),
(182, 'RU', 'Russian Federation', NULL),
(183, 'RW', 'Rwanda', NULL),
(184, 'KN', 'Saint Kitts and Nevis', NULL),
(185, 'LC', 'Saint Lucia', NULL),
(186, 'VC', 'Saint Vincent and the Grenadines', NULL),
(187, 'WS', 'Samoa', NULL),
(188, 'SM', 'San Marino', NULL),
(189, 'ST', 'Sao Tome and Principe', NULL),
(190, 'SA', 'Saudi Arabia', NULL),
(191, 'SN', 'Senegal', NULL),
(192, 'RS', 'Serbia', NULL),
(193, 'SC', 'Seychelles', NULL),
(194, 'SL', 'Sierra Leone', NULL),
(195, 'SG', 'Singapore', NULL),
(196, 'SK', 'Slovakia', NULL),
(197, 'SI', 'Slovenia', NULL),
(198, 'SB', 'Solomon Islands', NULL),
(199, 'SO', 'Somalia', NULL),
(200, 'ZA', 'South Africa', NULL),
(201, 'GS', 'South Georgia South Sandwich Islands', NULL),
(202, 'ES', 'Spain', NULL),
(203, 'LK', 'Sri Lanka', NULL),
(204, 'SH', 'St. Helena', NULL),
(205, 'PM', 'St. Pierre and Miquelon', NULL),
(206, 'SD', 'Sudan', NULL),
(207, 'SR', 'Suriname', NULL),
(208, 'SJ', 'Svalbard and Jan Mayen Islands', NULL),
(209, 'SZ', 'Swaziland', NULL),
(210, 'SE', 'Sweden', NULL),
(211, 'CH', 'Switzerland', NULL),
(212, 'SY', 'Syrian Arab Republic', NULL),
(213, 'TW', 'Taiwan', NULL),
(214, 'TJ', 'Tajikistan', NULL),
(215, 'TZ', 'Tanzania, United Republic of', NULL),
(216, 'TH', 'Thailand', NULL),
(217, 'TG', 'Togo', NULL),
(218, 'TK', 'Tokelau', NULL),
(219, 'TO', 'Tonga', NULL),
(220, 'TT', 'Trinidad and Tobago', NULL),
(221, 'TN', 'Tunisia', NULL),
(222, 'TR', 'Turkey', NULL),
(223, 'TM', 'Turkmenistan', NULL),
(224, 'TC', 'Turks and Caicos Islands', NULL),
(225, 'TV', 'Tuvalu', NULL),
(226, 'UG', 'Uganda', NULL),
(227, 'UA', 'Ukraine', NULL),
(228, 'AE', 'United Arab Emirates', NULL),
(229, 'GB', 'United Kingdom', NULL),
(230, 'US', 'United States', NULL),
(231, 'UM', 'United States minor outlying islands', NULL),
(232, 'UY', 'Uruguay', NULL),
(233, 'UZ', 'Uzbekistan', NULL),
(234, 'VU', 'Vanuatu', NULL),
(235, 'VA', 'Vatican City State', NULL),
(236, 'VE', 'Venezuela', NULL),
(237, 'VN', 'Vietnam', NULL),
(238, 'VG', 'Virgin Islands (British)', NULL),
(239, 'VI', 'Virgin Islands (U.S.)', NULL),
(240, 'WF', 'Wallis and Futuna Islands', NULL),
(241, 'EH', 'Western Sahara', NULL),
(242, 'YE', 'Yemen', NULL),
(243, 'ZR', 'Zaire', NULL),
(244, 'ZM', 'Zambia', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `main_course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `main_course`, `title`, `details`, `image`, `duration`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cultural', 'Vero ipsam nihil dol', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 'At minima lorem ipsa', 67.00, 1, '2019-08-18 00:19:29', '2019-08-18 00:19:29'),
(2, 'Math', 'Quos natus iste iust', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 'Laboriosam ipsum in', 521.00, 1, '2019-08-18 00:19:37', '2019-08-18 00:19:37'),
(3, 'Nepali', 'Reprehenderit ex eos', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 'Quos ad voluptates d', 490.00, 1, '2019-08-18 00:20:00', '2019-08-18 00:20:00'),
(4, 'Cultural', 'Repudiandae quod a m', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 'Est minus dolor qui', 543.00, 1, '2019-08-18 00:26:43', '2019-08-18 00:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `event_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_countdown` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `page`, `labelid`, `value`, `created_at`, `updated_at`) VALUES
(1, 'global', 'global:mobile', '(+44) 07727130053', '2019-07-17 01:44:46', '2019-07-17 02:13:10'),
(2, 'global', 'global:email', 'info@wancs.org.uk', '2019-07-17 01:45:16', '2019-08-28 23:27:14'),
(5, 'global', 'global:welcome_text', 'Welcome To Wisdom Academy', '2019-08-15 00:55:28', '2019-08-15 00:55:28'),
(6, 'global', 'global:testimonial_detail', 'We thanks for all our awesome reviews! There are hundreds of our happy customers! \r\n\r\nLet\'s see what others say about GoodWEB Solutions website template!', '2019-08-18 23:59:52', '2019-08-19 01:48:14'),
(7, 'global', 'global:testimonial_title', 'Reviews', '2019-08-19 00:01:45', '2019-08-19 01:47:58'),
(8, 'global', 'global:course_section_title', 'Our Courses', '2019-08-19 00:02:23', '2019-08-19 00:02:23'),
(9, 'global', 'global:course_section_detail', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel praesentium expedita placeat, dicta eum eos debitis veniam ...', '2019-08-19 00:02:39', '2019-08-19 00:02:39'),
(10, 'global', 'global:counter1', 'Teachers', '2019-08-19 01:06:49', '2019-08-19 01:06:49'),
(11, 'global', 'global:counter1_value', '5000', '2019-08-19 01:07:14', '2019-08-19 01:11:49'),
(12, 'global', 'global:counter2', 'Courses', '2019-08-19 01:12:53', '2019-08-19 01:12:53'),
(13, 'global', 'global:counter2_value', '15', '2019-08-19 01:13:13', '2019-08-19 01:13:13'),
(14, 'global', 'global:counter3', 'Students', '2019-08-19 01:13:48', '2019-08-19 01:13:48'),
(15, 'global', 'global:counter3_value', '1000', '2019-08-19 01:14:02', '2019-08-19 01:14:02'),
(18, 'global', 'global:address', 'PO Box 16122 Collins Street West  Victoria 8007 Australia', '2019-08-19 01:29:38', '2019-08-19 01:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_07_18_060744_create_posts_table', 1),
(2, '2019_07_18_075738_create_events_table', 2),
(3, '2019_07_18_083046_create_videos_table', 3),
(4, '2019_07_18_083929_create_albums_table', 4),
(5, '2019_07_18_083939_create_photos_table', 4),
(6, '2019_07_19_102339_create_ads_table', 5),
(7, '2019_07_22_065322_create_articles_table', 6),
(8, '2019_07_22_074030_create_downloads_table', 7),
(9, '2019_07_22_100943_create_volunteer_registers_table', 8),
(10, '2019_08_15_074213_create_blogs_table', 9),
(13, '2019_08_15_083504_create_courses_table', 10),
(14, '2019_08_21_090024_create_class_books_table', 11),
(15, '2019_08_28_044759_create_students_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `order_by` int(11) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `footer_order` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci,
  `meta_keyword` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `title`, `slug`, `overview`, `details`, `image`, `status`, `order_by`, `menu_order`, `footer_order`, `position`, `meta_title`, `meta_keyword`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 0, 'Home', '/', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 1, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2019-08-18 05:47:43', '2019-08-18 05:47:43'),
(2, 0, 'About', 'about', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 1, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2019-08-18 05:48:33', '2019-08-18 05:48:33'),
(3, 0, 'Courses', 'courses', NULL, '<p>Courses</p>', NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-08-18 05:49:03', '2019-08-18 05:49:03'),
(4, 0, 'Gallery', 'gallery', NULL, '<p>Gallery</p>', NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-08-18 05:49:47', '2019-08-18 05:49:47'),
(5, 0, 'Terms & Conditions', 'terms-conditions', NULL, '<p>Terms &amp; Conditions</p>', NULL, 1, NULL, NULL, NULL, 2, NULL, NULL, NULL, '2019-08-18 05:50:19', '2019-08-18 05:50:19'),
(6, 0, 'Blogs', 'blogs', NULL, '<p>Blogs</p>', NULL, 1, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2019-08-18 05:50:37', '2019-08-18 05:50:37'),
(7, 0, 'Reviews', 'reviews', NULL, '<p>Reviews</p>', NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-08-18 05:50:53', '2019-08-18 05:50:53'),
(8, 0, 'Notice', 'notice', NULL, '<p>Notice</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:51:19', '2019-08-18 05:51:48'),
(9, 0, 'Contact Us', 'contact-us', NULL, '<p>Contact Us</p>', NULL, 1, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2019-08-18 05:51:36', '2019-08-18 05:51:36'),
(10, 3, 'Nepali', 'nepali', NULL, '<p>Nepali</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:52:36', '2019-08-18 05:52:36'),
(11, 3, 'Math', 'math', NULL, '<p>Math</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:52:50', '2019-08-18 05:52:50'),
(12, 3, 'Cultural', 'cultural', NULL, '<p>Cultural</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:53:00', '2019-08-18 05:53:00'),
(13, 4, 'Photo Gallery', 'photo-gallery', NULL, '<p>Photo Gallery</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:53:15', '2019-08-18 05:53:15'),
(14, 4, 'Video Gallery', 'video-gallery', NULL, '<p>Video Gallery</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:53:26', '2019-08-18 05:53:26'),
(15, 8, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua', 'lorem-ipsum-dolor-sit-amet-consetetur-sadipscing-elitr-sed-diam-nonumy-eirmod-tempor-invidunt-ut-labore-et-dolore-magna-aliquyam-erat-sed-diam-voluptua', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-08-18 05:54:01', '2019-08-18 05:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `album_id`, `image`, `caption`, `status`, `created_at`, `updated_at`) VALUES
(3, 2, '3.jpeg', NULL, 1, '2019-08-15 02:42:56', '2019-08-15 02:42:56'),
(4, 2, '4.jpeg', NULL, 1, '2019-08-15 02:42:56', '2019-08-15 02:42:56'),
(5, 2, '5.jpeg', NULL, 1, '2019-08-15 02:42:56', '2019-08-15 02:42:56'),
(6, 2, '6.jpeg', NULL, 1, '2019-08-15 02:42:56', '2019-08-15 02:42:56'),
(7, 2, '7.jpeg', NULL, 1, '2019-08-15 02:42:56', '2019-08-15 02:42:56'),
(8, 3, '8.jpeg', NULL, 1, '2019-08-19 04:00:59', '2019-08-19 04:00:59'),
(9, 3, '9.jpeg', NULL, 1, '2019-08-19 04:00:59', '2019-08-19 04:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `title`, `name`, `email`, `image`, `details`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Accusantium ad offic', 'Julie Long', 'garepyti@mailinator.com', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, '2019-08-15 01:42:04', '2019-08-15 01:42:04'),
(2, 'Eos autem consequunt', 'Dominic Martin', 'texaf@mailinator.net', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, '2019-08-18 02:29:08', '2019-08-18 02:29:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `value`, `created_at`, `updated_at`) VALUES
(1, 'emails', 'info@wancs.org.uk;', '2018-10-07 05:01:33', '2019-08-28 23:50:59'),
(2, 'facebook_page', 'https://www.facebook.com/Namaste-Chicago-110468186969972/', '2019-08-18 05:56:20', '2019-08-18 05:56:20');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `link` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `order_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `package_id`, `title`, `details`, `image`, `link`, `status`, `order_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'Slide 1', NULL, 'slide-1-1.jpeg', NULL, 1, 1, '2019-08-15 00:51:45', '2019-08-15 00:51:45'),
(2, 0, 'Slide 2', NULL, 'slide-2-2.jpeg', NULL, 1, 2, '2019-08-15 00:51:58', '2019-08-15 00:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `title`, `icon`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'facebook', 'facebook', '#', 1, '2019-07-17 01:46:33', '2019-07-17 01:46:33'),
(2, 'twitter', 'twitter', '#', 1, '2019-07-17 01:47:00', '2019-07-17 01:47:00');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_email` text COLLATE utf8mb4_unicode_ci,
  `parent_address` text COLLATE utf8mb4_unicode_ci,
  `parent_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `groups` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `groups`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 100, 'admin', 'admin@wancs.org.uk', '$2y$10$VB2ijjSdCm6i1L.6EC4J5uwFyD6CkcEv3PbB5fcgNUfbE8B1tWTZO', 'uxuT4f2WqFnlBlSPqtXgBnf69yRcQPNBgL3qmVCNYoAfMfWyvHAutLXlCBa3', '2018-01-09 00:54:37', '2019-08-29 03:23:33');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Secrets', 'https://www.youtube.com/watch?v=qHm9MG9xw1o&list=RDkVpv8-5XWOI&index=16', 1, '2019-08-15 01:06:48', '2019-08-15 01:06:48'),
(2, 'Reviews', 'https://www.youtube.com/watch?v=qHm9MG9xw1o&list=RDkVpv8-5XWOI&index=16', 1, '2019-08-19 03:56:32', '2019-08-19 03:56:32'),
(3, 'Zumba', 'https://www.youtube.com/watch?v=qHm9MG9xw1o&list=RDkVpv8-5XWOI&index=16', 1, '2019-08-19 03:56:37', '2019-08-19 03:56:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_books`
--
ALTER TABLE `class_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191)),
  ADD KEY `name_2` (`name`(191));

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_books`
--
ALTER TABLE `class_books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
