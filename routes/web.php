<?php

Route::get('/', 'PageController@home')->name('_root_');


// routes for admin
Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\LoginController@login');
Route::get('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');
Route::post('admin/logout', 'Admin\LoginController@logout');
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function(){
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/phpinfo', 'DashboardController@phpinfo');
    Route::resource('pages', 'PageController');
    Route::post('pages/order', 'PageController@order')->name('pages.order');
    Route::get('pages/showhide/{page}', 'PageController@showhide')->name('pages.showhide');
    Route::get('pages/photodelete/{page}', 'PageController@photodelete')->name('pages.photodelete');
    Route::resource('ads', 'AdController');
    Route::get('ads/showhide/{ad}', 'AdController@showhide')->name('ads.showhide');
    Route::post('ads/order', 'AdController@order')->name('ads.order');
    Route::resource('slides', 'SlideController');
    Route::post('slides/order', 'SlideController@order')->name('slides.order');
    Route::get('slides/showhide/{slide}', 'SlideController@showhide')->name('slides.showhide');
    Route::resource('labels', 'LabelController');
    Route::resource('settings', 'SettingController');
    Route::resource('reviews', 'ReviewController');
    Route::get('reviews/approve/{review}', 'ReviewController@approve')->name('reviews.approve');
    Route::get('reviews/cancel/{review}', 'ReviewController@cancel')->name('reviews.cancel');
    Route::resource('socials', 'SocialController');
    Route::resource('admins', 'AdminController');
    Route::resource('posts','PostController');
    Route::get('posts/showhide/{slide}', 'PostController@showhide')->name('posts.showhide');
    Route::resource('events', 'EventController');
    Route::get('events/photodelete/{event}', 'EventController@photodelete')->name('events.photodelete');
    Route::resource('videos','VideoController');
    Route::resource('albums','AlbumController');
    Route::post('albums/photodelete', 'AlbumController@photodelete')->name('albums.photodelete');
    Route::resource('blogs','BlogController');
    Route::resource('courses','CourseController');
    Route::get('course-application','CourseApplicationController@index')->name('course.application');
    Route::get('course-application/{id}','CourseApplicationController@view')->name('course.application.view');
    Route::get('password/change', 'DashboardController@passwordchange')->name('password.change');
    Route::post('password/change', 'DashboardController@passwordstore');
    Route::resource('students','StudentController');
    Route::resource('teachers','TeacherController');
    Route::resource('documents', 'DocumentController');
});


Route::group(['middleware' => ['auth']], function(){
    Route::get('profile', 'UserController@profile')->name('profile');
});

Auth::routes();

$router = app()->make('router');
$pages = App\Page::all();
foreach ($pages as $page) {
    if($page->slug == 'contact-us'){
        $router->get($page->slug, 'PageController@contact');
        $router->post($page->slug, 'PageController@contactmail')->name('contactmail');
    } elseif($page->slug == 'reviews'){
        $router->get($page->slug, 'PageController@review');
    } elseif($page->slug == 'news-updates'){
        $router->get($page->slug, 'PageController@newslist');
    } elseif($page->slug == 'courses'){
        $router->get($page->slug, 'PageController@courses')->name('courses');
    } elseif($page->slug == 'gallery'){
        $router->get($page->slug, 'PageController@gallery')->name('gallery');
    } elseif ($page->slug == 'video-gallery') {
        $router->get($page->slug, 'PageController@video')->name('video');
    } elseif ($page->slug == 'photo-gallery') {
        $router->get($page->slug, 'PageController@photogallery')->name('photo.gallery');
    } elseif ($page->slug == 'blogs') {
        $router->get($page->slug, 'PageController@blogs');
    } elseif ($page->slug == 'notice') {
        $router->get($page->slug, 'PageController@notice');
    }
    elseif ($page->slug == 'event') {
        $router->get($page->slug, 'PageController@event');}
    elseif ($page->slug == 'teachers') {
        $router->get($page->slug, 'PageController@teachers');
    } elseif ($page->slug == 'document') {
        $router->get($page->slug, 'PageController@document')->name('document');
        $router->post($page->slug, 'PageController@document_auth');
    }elseif($page->slug <> '/'){
        $router->get($page->slug, 'PageController@index');
    }
}

Route::post('class-book','ClassBookingController@book')->name('class.book');
Route::get('gallery/photo-gallery/{slug}', 'PageController@singlegallery')->name('single.gallery');
Route::post('reviews', 'PageController@reviewsave')->name('review');
Route::get('news-updates/{slug}','PageController@singlenews')->name('singlenews');
Route::get('blogs/{slug}','PageController@singleblog')->name('single.blog');
Route::get('events/{id}','PageController@singleevent')->name('single.event');
Route::get('courses/{slug}/{title}','PageController@singlecourse')->name('single.course');
Route::get('captcha.jpg', 'CaptchaController@image')->name('captcha');

Route::get('thankyou', 'PageController@thankyou')->name('thankyou');


//Preview email on brower
Route::get('emailview/{id}', function($id) {
    $tablebook = \App\Order::find($id);
    return new \App\Mail\NewOrder($tablebook);
});
