<?php 
return [

	'header' => [
		'title' => 'Header ad',
		'helper_txt' => 'This ad is placed in header',
		'width' => '1140',
		'height' => '100'
	],
	'sidebar' => [
		'title' => 'Sidebar ad',
		'helper_txt' => 'This ad is placed in sidebar',
		'width' => '1140',
		'height' => '100'
	],

];
