<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $attributes = [
    	'status' => 1,
    ];

    protected $fillable = [
    	'title', 'slug', 'image', 'details',
    ];

    public function image($size = '450x300')
	{
		if($this->image){
			$image = public_path('uploads/posts/' . $this->image);
			if (file_exists($image)) {
				return asset('uploads/posts/' . $this->image);
			}else{				
				return 'holder.js/' . $size . '/image not found';
			}
		}else{			
			return 'holder.js/' . $size . '/image not found';
		}
	}
}
