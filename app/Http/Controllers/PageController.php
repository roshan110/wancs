<?php

// ALTER TABLE `registers` ADD `program` TEXT NULL AFTER `message`, ADD `price` FLOAT(8,2) NULL AFTER `program`;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Slide;
use App\Review;
use App\Post;
use App\Video;
use App\Event;
use App\Album;
use App\Photo;
use App\Blog;
use App\Course;
use App\Teacher;
use App\Document;
use App\Setting;

use Mail;
use App\Mail\Contact;

class PageController extends Controller
{
    public function home()
    {
        $page = Page::find(1);
        if(!$page){
            $page = Page::where('slug', '/')->first();
        }
        $data['page'] = $page;
        $data['slides'] = Slide::active()->get();
        $data['video'] = Video::latest()->first();
        $data['album'] = Album::latest()->first();
        $data['reviews'] = Review::latest()->take(3)->get();
        $data['notice'] = Page::where('parent_id', 31)->latest()->active()->first();
        $data['nepali_course'] = Page::find(10);
        $data['math_course'] = Page::find(11);
        $data['cultural_course'] = Page::find(12);
        // $data['first_post'] = Post::where('status', 1)->latest()->first();
        // $data['posts'] = Post::where('status', 1)->whereNotIn('id', [$data['first_post']->id])->latest()->take(3)->get();
        $data['event'] = Event::where('show_in_countdown',1)->latest()->first();
        return view('index', $data);
    }

    public function index(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        return view('page', $data);
    }

    public function contact(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        return view('contact', $data);
    }

    public function contactmail(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'captcha' => 'required',
        ]);
        if(\Session::get('rand_pass') <> $request->captcha){
            return redirect()->back()->withErrors(['captcha' => 'Invalid Cpatcha'])->withInput();
        }
        Mail::send(new Contact($request));
        return redirect()->route('thankyou');
    }

    public function thankyou()
    {
        return view('thankyou');
    }

    public function review(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['reviews'] = Review::active()->get();
        return view('review', $data);
    }

    public function reviewsave(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'title'=>'required',
            'email' => 'required',
            'image' => 'image|max:1024',
            'review' => 'required',
            'captcha' => 'required',
        ]);

        if(\Session::get('rand_pass') <> $request->captcha){
            return redirect()->back()->withErrors(['captcha' => 'Invalid Cpatcha'])->withInput();
        }

        $review = new review;
        $review->title=$request->title;
        $review->name = $request->name;
        $review->email = $request->email;
        $review->details = $request->review;
        $review->status = 0;
        $review->save();

        if ($request->hasFile('image')) {
            $image_name = 'review-'.$review->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/reviews/', $image_name);
            $review->image = $image_name;
            $review->save();
        }

        return redirect(url('reviews'))->with('success', 'thank you');

    }

    public function newslist(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['posts'] = Post::all();
        return view('news_updates.newslist', $data);
    }

    public function singlenews($slug)
    {
        $data['post'] = Post::where('slug',$slug)->first();
        return view('news_updates.singlenews', $data);
    }
    public function event(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
//        $data['events'] = Event::all();
        $data['events'] = Event::whereRaw(\DB::raw('event_date >= CURDATE()'))->orderBy('event_date')->get();
        return view('events.events', $data);
    }
    public function singleevent($id)
    {
        $data['event'] = Event::find($id);
        return view('events.singleevent', $data);
    }

    public function gallery(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['albums'] = Album::all();
        $data['videos'] = Video::all();
        return view('albums.albumlist',$data);
    }

    public function singlegallery($slug)
    {
        $album = Album::where('slug',$slug)->first();
        $photos = Photo::where('album_id',$album->id)->paginate(10);
        return view('albums.singlealbum',compact('album','photos'));
    }

    public function blogs(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['blogs'] = Blog::latest()->get();
        return view('blogs.bloglist', $data);
    }

    public function singleblog($slug)
    {
        $data['blog'] = Blog::where('slug',$slug)->first();
        return view('blogs.singleblog',$data);
    }

    public function courses(Request $request)
    {
        $data['courses'] = Course::latest()->get();
        $data['nepali_course'] = Course::where('main_course','Nepali')->count();
        $data['math_course'] = Course::where('main_course','Math')->count();
        $data['cultural_course'] = Course::where('main_course','Cultural')->count();
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        return view('courses.courselist', $data);
    }

    public function singlecourse($slug,$title)
    {
        $data['course'] = Course::where('title',$title)->first();
        return view('courses.singlecourse',$data);
    }

    public function video(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        $data['videos'] = Video::all();
        return view('video', $data);
    }

    public function notice(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        return view('notice', $data);
    }

    public function teachers(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        $data['teachers'] = Teacher::latest()->get();
        return view('teacher', $data);
    }
    public function document(Request $request)
    {
        $data['doc_auth'] = $request->session()->has('doc_auth');
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        if(!empty(session('course'))) {
            $course_name = session('course');
            $data['documents'] = Document::where('course',$course_name)->get();
        }
        else
        {
            $data['documents']=Document::all();
        }
//        dd($course_name);


        return view('document', $data);
    }

    public function document_auth(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
        ]);

        $math_doc_password = Setting::ofValue('math_doc_password');
        $nepali_doc_password = Setting::ofValue('nepali_doc_password');
        $cultural_doc_password = Setting::ofValue('cultural_doc_password');


        if ($math_doc_password === $request->password) {
            $request->session()->put('doc_auth', true);
            $request->session()->push('course', 'Math');
            return redirect()->route('document');
        }
        elseif($nepali_doc_password === $request->password)
        {
            $request->session()->put('doc_auth', true);
            $request->session()->push('course', 'Nepali');
            return redirect()->route('document');
        }
        elseif($cultural_doc_password === $request->password){
            $request->session()->put('doc_auth', true);
            $request->session()->push('course', 'Cultural');
            return redirect()->route('document');
        }
        else {
            return redirect()->back()->withErrors(['password' => 'Password Mismatch']);
        }
    }
}

