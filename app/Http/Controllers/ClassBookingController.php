<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassBook;
use Mail;
use App\Mail\ClassBooking;
use App\Mail\AutoResponse;

class ClassBookingController extends Controller
{
    public function book(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'captcha' => 'required',
        ]);
        if(\Session::get('rand_pass') <> $request->captcha){
            return redirect()->back()->withErrors(['captcha' => 'Invalid Cpatcha'])->withInput();
        }

        $data = array(
        	'name' => $request->name,
        	'email' => $request->email,
        	'phone' => $request->phone,
        	'address' => $request->address,
        	'course_id' => $request->course_id,
        );
        ClassBook::create($data);
        Mail::send(new ClassBooking($request));
        Mail::send(new AutoResponse($request));
        return redirect()->route('thankyou');
    }
}
