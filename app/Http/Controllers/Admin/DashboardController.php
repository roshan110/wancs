<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\ClassBook;

class DashboardController extends Controller
{
    public function index()
    {
        $data['applications'] = ClassBook::latest()->take(10)->get();
    	return view('admin.dashboard', $data);
    }

    public function passwordchange()
    {
    	return view('admin.changepassword');
    }

    public function passwordstore(Request $request)
    {
    	$this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
		]);
		
		$user = \Auth::user();
		if (Hash::check($request->old_password, $user->password)) {
			$user->password = Hash::make($request->password);
			$user->save();
			\Session::flash('success', 'Password Changed');
			return redirect('admin');
		}else{
			\Session::flash('error', 'Password does not match');
			return redirect('admin');
		}
    }

    public function phpinfo()
    {
    	phpinfo();
    }

}
