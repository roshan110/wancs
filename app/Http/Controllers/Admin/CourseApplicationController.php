<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClassBook;

class CourseApplicationController extends Controller
{
    public function index()
    {
    	$data['applications'] = ClassBook::latest()->paginate(10);
    	return view('admin.course_application.index', $data);
    }

    public function view($id)
    {
    	$data['application'] = ClassBook::find($id);
    	return view('admin.course_application.view', $data);
    }
}
