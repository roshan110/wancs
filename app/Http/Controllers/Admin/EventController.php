<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('created_at', 'desc')->get();
        return view('admin.event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'event_place' => 'required',
            'event_date' => 'required',
        ]);

        $show_in_countdown = 0;
        if($request->show_in_countdown){
            $show_in_countdown = 1;
        }

        $event = new Event;
        $event->title = $request->title;
        $event->details = $request->details;
        $event->event_place = $request->event_place;
        $event->event_date = $request->event_date;
        $event->end_date = $request->end_date;
        $event->show_in_countdown = $show_in_countdown;
        $event->save();

        if ($request->hasFile('image')) {
            $image_name = str_slug($request->title) . '-'.$event->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/events/', $image_name);
            $event->image = $image_name;
            $event->save();
        }

        return redirect()->route('events.index')->with('success', 'event added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('admin.event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $this->validate($request, [
            'title' => 'required',
            'event_place' => 'required',
            'event_date' => 'required',            
        ]);

        $show_in_countdown = 0;
        if($request->show_in_countdown){
            $show_in_countdown = 1;
        }

        $event->title = $request->title;
        $event->details = $request->details;
        $event->event_place = $request->event_place;
        $event->event_date = $request->event_date;
        $event->end_date = $request->end_date; 
        $event->show_in_countdown = $show_in_countdown;    
        $event->save();

        if ($request->hasFile('image')) {
            if($event->image && file_exists(public_path('uploads/events/'.$event->image))){
                unlink(public_path('uploads/events/'.$event->image));
            }
            $image_name = str_slug($request->title) . '-'.$event->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/events/', $image_name);
            $event->image = $image_name;
            $event->save();
        }

        return redirect()->route('events.index')->with('success', 'event saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if($event){

            if($event->image){
                $image = public_path('uploads/events/' . $event->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($event->delete()){
                return redirect()->route('events.index')->with('success', 'event deleted.');
            }else{
                return redirect()->route('events.index')->with('error', 'Error while deleting event.');
            }

        }else{
            abort();
        }
        
    }

    public function photodelete(Event $event)
    {
        if($event && $event->image)
        {
            if(file_exists(public_path('/uploads/events/'.$event->image)))
            {
                unlink(public_path('/uploads/events/'.$event->image));
            }
            $event->image = null;
            $event->save();
        }
        return redirect()->back()->with('success','Image Deleted');
    }
}
