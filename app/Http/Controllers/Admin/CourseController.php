<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['courses'] = Course::all();
        return view('admin.course.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'main_course' => 'required',
            'title' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $data = array(
            'main_course' => $request->main_course,
            'title' => $request->title,
            'details' => $request->details,
            'duration' => $request->duration,
            'price' => $request->price,
        );

        $course = Course::create($data);
        if ($request->hasFile('image')) {
            $image_name = $course->id . '-'.$course->title . '.'.$request->image->extension();
            $path = $request->image->move('uploads/courses/', $image_name);
            $course->image = $image_name;
            $course->save();
        }

        return redirect()->route('courses.index')->with('success','Course Added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('admin.course.edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $this->validate($request, [
            'main_course' => 'required',
            'title' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $data = array(
            'main_course' => $request->main_course,
            'title' => $request->title,
            'details' => $request->details,
            'duration' => $request->duration,
            'price' => $request->price,
        );

        Course::where('id',$course->id)->update($data);

        if ($request->hasFile('image')) {
            if($course->image && file_exists(public_path('uploads/courses/'.$course->image))){
                unlink(public_path('uploads/courses/'.$course->image));
            }
            $image_name = $course->id . '-'.$course->title . '.'.$request->image->extension();
            $path = $request->image->move('uploads/courses/', $image_name);
            $course->image = $image_name;
            $course->save();
        }

        return redirect()->route('courses.index')->with('success','Course Saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        if($course){

            if($course->image){
                $image = public_path('uploads/courses/' . $course->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($course->delete()){
                return redirect()->route('courses.index')->with('success', 'course deleted.');
            }else{
                return redirect()->route('courses.index')->with('error', 'Error while deleting course.');
            }

        }else{
            abort();
        }
    }
}
