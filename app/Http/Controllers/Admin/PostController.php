<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['posts'] = Post::paginate(10);
        return view('admin.posts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'mimes:jpg,png,jpeg',
            'details' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
        );

        $post = Post::create($data);

        if($request->hasFile('image'))
        {
            $image_name = str_slug($request->title).'-'.$post->id.'.'.$request->image->extension();
            $path = $request->image->move('uploads/posts/', $image_name);
            $post->image = $image_name;
            $post->save();
        }

        return redirect()->route('posts.index')->with('success', 'Post added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'mimes:jpg,png,jpeg',
            'details' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
        );

        Post::where('id',$post->id)->update($data);

        if($request->hasFile('image'))
        {
            if($post->image && file_exists(public_path('uploads/posts/'.$post->image))){
                unlink(public_path('uploads/posts/'.$post->image));
            }
            $image_name = str_slug($request->title).'-'.$post->id.'.'.$request->image->extension();
            $path = $request->image->move('uploads/posts/', $image_name);
            $post->image = $image_name;
            $post->save();
        }

        return redirect()->route('posts.index')->with('success', 'Post added.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if($post){

            if($post->image){
                $image = public_path('uploads/posts/' . $post->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($post->delete()){
                return redirect()->route('posts.index')->with('success', 'Post deleted.');
            }else{
                return redirect()->route('posts.index')->with('error', 'Error while deleting Post.');
            }

        }else{
            abort();
        }
    }

    public function showhide(Post $post)
    {
        if($post->status){
            $post->status = 0;
        }else{
            $post->status = 1;
        }
        $post->save();
        return redirect()->route('posts.index')->with('success', 'Status Update.');        
    }
}
