<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['teachers'] = Teacher::all();
        return view('admin.teacher.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
        );

        $teacher = Teacher::create($data);
        if ($request->hasFile('image')) {
            $image_name = $teacher->id . '-'.$teacher->name . '.'.$request->image->extension();
            $path = $request->image->move('uploads/teachers/', $image_name);
            $teacher->image = $image_name;
            $teacher->save();
        }

        return redirect()->route('teachers.index')->with('success','Teacher Added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('admin.teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
        );

        Teacher::where('id',$teacher->id)->update($data);
        
        if ($request->hasFile('image')) {
            if($teacher->image && file_exists(public_path('uploads/teachers/'.$teacher->image))){
                unlink(public_path('uploads/teachers/'.$teacher->image));
            }
            $image_name = $teacher->id . '-'.$teacher->name . '.'.$request->image->extension();
            $path = $request->image->move('uploads/teachers/', $image_name);
            $teacher->image = $image_name;
            $teacher->save();
        }

        return redirect()->route('teachers.index')->with('success','Teacher Saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        if($teacher){

            if($teacher->image){
                $image = public_path('uploads/teachers/' . $teacher->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($teacher->delete()){
                return redirect()->route('teachers.index')->with('success', 'Teacher deleted.');
            }else{
                return redirect()->route('teachers.index')->with('error', 'Error while deleting teacher.');
            }

        }else{
            abort();
        }
    }
}
