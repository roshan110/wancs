<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['blogs'] = Blog::latest()->paginate(10);
        return view('admin.blog.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
        ]);

        $data = array(
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,
        );

        $blog = Blog::create($data);
        if($request->hasFile('image'))
        {
            $image_name = str_slug($request->title).'-'.$blog->id.'.'.$request->image->extension();
            $path = $request->image->move('uploads/blogs/', $image_name);
            $blog->image = $image_name;
            $blog->save();
        }

        return redirect()->route('blogs.index')->with('success','Blog Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('admin.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
        ]);

        $data = array(
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,
        );

        Blog::where('id', $blog->id)->update($data);
        if($request->hasFile('image'))
        {
            if($blog->image && file_exists(public_path('uploads/blogs/'. $blog->image)))
            {
                unlink(public_path('uploads/blogs/'.$blog->image));
            }
            $image_name = str_slug($request->title).'-'.$blog->id.'.'.$request->image->extension();
            $path = $request->image->move('uploads/blogs/', $image_name);
            $blog->image = $image_name;
            $blog->save();
        }

        return redirect()->route('blogs.index')->with('success','Blog Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        if($blog)
        {
            if($blog->image && file_exists(public_path('uploads/blogs/'. $blog->image)))
            {
                unlink(public_path('uploads/blogs/'.$blog->image));
            }
            if($blog->delete())
            {
                return redirect()->route('blogs.index')->with('success','Blog Deleted');
            }
            else
            {
                return redirect()->route('blogs.index')->with('error','Something went wrong while deleting blog');
            }
        }
        else
        {
            abort();
        }
    }
}
