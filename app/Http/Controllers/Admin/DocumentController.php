<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document;
use Vmorozov\FileUploads\Uploader;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['documents'] = Document::all();
        return view('admin.document.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['courses']=Course::all();
        return view('admin.document.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $this->validate($request, [
            'course'=>'required',
            'title' => 'required',
            'document' => 'required',
        ]);
       
        $data = array(
            'course' => $request->course,

            'title' => $request->title,

            'details' => $request->details,
        );

        $doc = Document::create($data);
        if ($request->hasFile('document')) {
            $document_name = str_slug($request->title) . '-'.$doc->id . '.'.$request->document->extension();
            $path = $request->document->move('uploads/document/', $document_name);
            $doc->document = $document_name;
            $doc->save();
        }

        return redirect()->route('documents.index')->with('success','Document Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        return view('admin.document.edit',compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'course' => $request->course,
            'details' => $request->details,
        );

        Document::where('id',$document->id)->update($data);
        if ($request->hasFile('document')) {
            if($document->document && file_exists(public_path('uploads/document/'.$document->document))){
                unlink(public_path('uploads/document/'.$document->document));
            }
            $document_name = str_slug($request->title) . '-'.$document->id . '.'.$request->document->extension();
            $path = $request->document->move('uploads/document/', $document_name);
            $document->document = $document_name;
            $document->save();
        }
        return redirect()->route('documents.index')->with('success','Document Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        if($document){
            if($document->document && file_exists(public_path('uploads/document/'.$document->document))){
                unlink(public_path('uploads/document/'.$document->document));
            }
            if($document->delete()){
                return redirect()->route('documents.index')->with('success','Document Deleted');
            }
            else{
                return redirect()->route('documents.index')->with('error','Something went wrong');
            }
        }
        else{
            abort();
        }
    }
}
