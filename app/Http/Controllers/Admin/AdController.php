<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filter){
            if($request->filter=='sidebar'||$request->filter='inner_sidebar'){
                $order = $request->filter == 'inner_sidebar'? 'in_sidebar_order' : 'sidebar_order';
                $type = '%"' . $request->filter . '"%';        
                $ads = Ad::where('type', 'like', $type)->active()->orderBy($order)->get();
            }else{
                $type = '%"' . $request->filter . '"%';        
                $ads = Ad::where('type', 'like', $type)->active()->get();
            }
            
        }else{
            $ads = Ad::all();
        }
        return view('admin.ad.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $categories = Category::all();
        return view('admin.ad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',            
            'type' => 'required',
        ]);
        $ad = new Ad;
        $ad->title = $request->title;
        $ad->type = $request->type;
        $ad->url = $request->url;
        $ad->status = $request->status?:0;
        $ad->save();

        if ($request->hasFile('image')) {
            $image_name = 'ad-'.$ad->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/ads/', $image_name);
            $ad->image = $image_name;
            $ad->save();
        }

        return redirect()->route('ads.index')->with('success', 'Ad added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        return view('admin.ad.edit', compact('ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        $this->validate($request, [
            'title' => 'required',            
            'type' => 'required',
        ]);
        $ad->title = $request->title;
        $ad->type = $request->type;
        $ad->url = $request->url;
        $ad->status = $request->status?:0;
        $ad->save();

        if ($request->hasFile('image')) {
            $image_name = 'ad-'.$ad->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/ads/', $image_name);
            $ad->image = $image_name;
            $ad->save();
        }
        return redirect()->route('ads.index')->with('success', 'Ad saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        if($ad){

            if($ad->image){
                $image = public_path('uploads/ads/' . $ad->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($ad->delete()){
                return redirect()->route('ads.index')->with('success', 'Ad deleted.');
            }else{
                return redirect()->route('ads.index')->with('error', 'Error while deleting Ad.');
            }

        }else{
            abort();
        }
    }

    public function showhide(Ad $ad)
    {
        if($ad->status){
            $ad->status = 0;
        }else{
            $ad->status = 1;
        }
        $ad->save();
        return redirect()->route('ads.index')->with('success', 'Status Update.');        
    }

    public function order(Request $request)
    {
        $order = $request->order == 'inner_sidebar'? 'in_sidebar_order' : 'sidebar_order';
        foreach ($request->data as $odr => $id) {
            $odr = $odr + 1;
            $legaldoc = Ad::find($id);
            $legaldoc->$order = $odr;
            $legaldoc->save();
        }
        return 'changed';
    }
}
