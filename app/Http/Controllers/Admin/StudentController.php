<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['students'] = Student::all();
        return view('admin.student.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'parent_name' => 'required',
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'parent_name' => $request->parent_name,
            'parent_email' => $request->parent_email,
            'parent_address' => $request->parent_address,
            'parent_phone' => $request->parent_phone,
        );

        Student::create($data);
        return redirect()->route('students.index')->with('success','Student Detail Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('admin.student.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $this->validate($request, [
            'name' => 'required',
            'parent_name' => 'required',
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'parent_name' => $request->parent_name,
            'parent_email' => $request->parent_email,
            'parent_address' => $request->parent_address,
            'parent_phone' => $request->parent_phone,
        );

        Student::where('id', $student->id)->update($data);
        return redirect()->route('students.index')->with('success','Student Detail Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        if($student)
        {
            if($student->delete())
            {
                return redirect()->route('students.index')->with('success','Student Deleted');
            }
            else
            {
                return redirect()->route('students.index')->with('error','Something went wrong');
            }
        }
        else
        {
            abort();
        }
    }
}
