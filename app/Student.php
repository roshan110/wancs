<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $attributes = [
    	'status' => 1,
    ];

    protected $fillable = [ 
    	'name', 'email', 'address', 'phone', 'parent_name', 'parent_email', 'parent_address', 'parent_phone'
    ];
}
