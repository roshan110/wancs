<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $attributes = [
    	'status' => 1,
    ];

    protected $fillable = [
    	'title', 'slug', 'image', 'details', 'meta_title', 'meta_keyword', 'meta_description',
    ];

    public function image($size = '450x300')
	{
		if($this->image){
			$image = public_path('uploads/blogs/' . $this->image);
			if (file_exists($image)) {
				return asset('uploads/blogs/' . $this->image);
			}else{				
				return 'holder.js/' . $size . '/image not found';
			}
		}else{			
			return 'holder.js/' . $size . '/image not found';
		}
	}
}
