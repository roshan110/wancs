<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $attributes = [
	'status' => 1,
	];

    protected $casts = [
        'type' => 'array',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }


    public function getFTypeAttribute()
    {
    	$types = config('ads');
    	$f_type = null;

    	foreach ($this->type as $type) {
    		// $f_type .= ($f_type? ', ':null) . isset($types[$type]['title']) ? $types[$type]['title'] : nulll;
    		$f_type .= ($f_type? ', ':null) . $type;
    	}
    	return $f_type;
    }


    //static methods
    public static function has($type)
    {
    	$type = '%"' . $type . '"%';        
        $ads = self::where('type', 'like', $type)->active()->get();
    	return (count($ads) > 0);
    }

    public static function get($type, $offset=0, $limit=1000)
    {
    	$_type = '%"' . $type . '"%';
    	if($type == 'sidebar' || $type == 'inner_sidebar'){
			$order = $type == 'inner_sidebar'? 'in_sidebar_order' : 'sidebar_order';
    		$ads = self::where('type', 'like', $_type)->orderBy($order)->limit($limit)->offset($offset)->active()->get();
    		return $ads;
    	}else{
    		$ads = self::where('type', 'like', $_type)->latest()->active()->first();
    		return $ads;
    	}    	
    	
    }
}
