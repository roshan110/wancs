<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('main_menu', \App\Menu::main());
            $view->with('footer_menu', \App\Menu::footer());
            $view->with('socials', \App\Social::all());
            $view->with('about',\App\Page::find(2));
            $view->with('notice', \App\Page::where('parent_id',8)->first());
            $view->with('booking_courses', \App\Course::all());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
