<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\_image;

class Event extends Model
{
    protected $attributes = [
        'status' => 1,
    ];

    protected $casts = [
        'event_date' => 'date',
        'end_date' => 'date',
    ];


    public function getUrlAttribute(){
        return url('events/' . $this->id);
    }

    public function getFDateAttribute(){
        // if($this->created_at == ){

        // }
    }

    public function image($size = null)
    {
        if($size){
            $_image = asset('images/default/' . $size . '.jpg');
            $original = public_path('uploads/events/'. $this->image);


            if($this->image && file_exists($original)){
                $_image = asset('uploads/events/' . $size . '/' . $this->image);
//                $_image_path = public_path('uploads/events/' . $size . '/' . $this->image);
//                if(!file_exists($_image_path)){
//                    // $this->imageCompress($original);
//                    $_image = new  _image('events', $size, $this->image);
//                }
            }
        }else{
            $original = public_path('uploads/events/'. $this->image);
            if($this->image && file_exists($original)){
                $_image = asset('uploads/events/'. $this->image);
            }else{
                $_image = null;
            }
        }
        return $_image;
    }


}
