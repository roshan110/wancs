<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $attributes = [
    	'status' => 1,
    ];

    protected $fillable = [
    	'title', 'document', 'details','course',
    ];
}
