<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Setting;

class ClassBooking extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    public $emails;
    public $course;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $emails = Setting::ofValue('emails');
        $emails = array_filter(array_map('trim',explode(';', $emails)));
        $this->emails = $emails;
        $this->request = $request;
        $this->course = \App\Course::where('id', $this->request['course_id'])->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->request->email, $this->request->name)
        ->to($this->emails) 
        ->markdown('emails.classbooking');
    }
}
