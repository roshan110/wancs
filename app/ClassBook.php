<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassBook extends Model
{
    protected $attributes = [
    	'status' => 1,
    ];

    protected $fillable = [
    	'course_id', 'name', 'email', 'phone', 'address',
    ];

    public function course()
    {
    	return $this->belongsTo('App\Course','course_id');
    }
}
