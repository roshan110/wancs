<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $attributes = [
		'status' => 1,
	];

    protected $fillable = [
    	'main_course', 'title', 'details', 'image', 'duration', 'price',
    ];

    public function image($size = '450x300')
	{
		if($this->image){
			$image = public_path('uploads/courses/' . $this->image);
			if (file_exists($image)) {
				return asset('uploads/courses/' . $this->image);
			}else{				
				return asset('images/default-course.jpg');
			}
		}else{			
			return asset('images/default-course.jpg');
		}
	}

	public function classbook()
	{
		return $this->hasOne('App\ClassBook','course_id');
	}
}
