<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $attributes = [
    	'status' => 1,
    ];

    protected $fillable = [
    	'name', 'email', 'phone', 'address', 'image',
    ];

    public function image($size = '450x300')
	{
		if($this->image){
			$image = public_path('uploads/teachers/' . $this->image);
			if (file_exists($image)) {
				return asset('uploads/teachers/' . $this->image);
			}else{				
				return asset('images/default-user.png');
			}
		}else{			
			return asset('images/default-user.png');
		}
	}
}
